package org.garm.infrastructure.base.model.security;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CurrentUserInfoDto {

    private String currentFirstName;
    private String currentLastName;
    private String currentTitle;
    private String mobileNumber;
    private String currentUsername;
    private String currentNationalId;
    private String currentNationalCode;
    private boolean real;
    private Long currentUserId;
    private List<Long> currentUserGroups;
    private List<Long> currentUserRoles;
}
