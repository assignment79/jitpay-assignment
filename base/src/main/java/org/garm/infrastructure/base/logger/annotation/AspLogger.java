package org.garm.infrastructure.base.logger.annotation;

import org.garm.infrastructure.base.logger.GeneralLogger;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspLogger {

    @Autowired
    private GeneralLogger generalLogger;

    @AfterReturning(pointcut = "@annotation(org.garm.infrastructure.base.logger.annotation.Logger)",
            returning = "object")
    public void LogeEvents(Object object) {
        generalLogger.info(">> " + object.toString());
    }

}
