package org.garm.infrastructure.base.cache;

import org.garm.infrastructure.base.model.security.AuthDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class AuthCacheService extends CacheHandler<AuthDto> {

    @Autowired
    public AuthCacheService(RedisTemplate<String, Object> redisTemplate) {
        super("auth-", redisTemplate, AuthDto.class);
    }

}
