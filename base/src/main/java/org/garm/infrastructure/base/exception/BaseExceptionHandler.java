package org.garm.infrastructure.base.exception;

import org.garm.infrastructure.base.config.PropertiesHandler;
import org.garm.infrastructure.base.enums.ExceptionCode;
import org.garm.infrastructure.base.enums.ResponseCode;
import org.garm.infrastructure.base.model.service.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.TransientPropertyValueException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ValidationException;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@RequiredArgsConstructor
public class BaseExceptionHandler extends ResponseEntityExceptionHandler {

    private final StackTraceHandler stackTraceHandler;
    private final HibernateExceptionTranslator translator;
    private final Response<Object, Object> response = new Response<>();
    private final PropertiesHandler propertiesHandler = new PropertiesHandler();

    @ExceptionHandler(RuntimeException.class)
    protected ResponseEntity<Object> handleRuntimeException(RuntimeException ex) {
        log.error(ExceptionUtils.getStackTrace(ex));
        response.setErrorDetail(new ExceptionModel()
                .setMessage(propertiesHandler.getValue("error.null.pointer.exception"))
                .setStackTrace(stackTraceHandler.handle(ex)));
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(BaseException.class)
    protected ResponseEntity<Object> handleExceptionBase(BaseException ex) {
        log.error(ExceptionUtils.getStackTrace(ex));
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage() == null ? "" : propertiesHandler.getValue(ex.getMessage(), ex.getArguments()))
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ex.getCode())
                .setExceptionCode(ex.getExceptionCode()));
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    protected ResponseEntity<Object> handleMaxUploadSizeExceededException(MaxUploadSizeExceededException ex) {
        log.error(ExceptionUtils.getStackTrace(ex));
        response.setErrorDetail(new ExceptionModel()
                .setMessage(propertiesHandler.getValue("error.max.upload.size.exceeded.exception"))
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.INPUT_ARGUMENT_ERROR.getCode().toString())
                .setExceptionCode(ExceptionCode.INPUT_ARGUMENT_ERROR));
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(ValidationException.class)
    protected ResponseEntity<Object> handleException(ValidationException ex) {
        log.error(ExceptionUtils.getStackTrace(ex));
        response.setErrorDetail(new ExceptionModel()
                .setMessage(propertiesHandler.getValue("error.orm.01400.suggestion"))
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.INPUT_ARGUMENT_ERROR.getCode().toString())
                .setExceptionCode(ExceptionCode.INPUT_ARGUMENT_ERROR)
        );
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(ServiceException.class)
    protected ResponseEntity<Object> handleServiceException(ServiceException ex) {
        log.error(ExceptionUtils.getStackTrace(ex));
        response.setInputArguments(ex.getArguments());
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ex.getCode())
                .setExceptionCode(ex.getExceptionCode())
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex) {
        log.error(ExceptionUtils.getStackTrace(ex));
        response.setErrorDetail(new ExceptionModel()
                .setMessage(translator.translate(ex))
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.INPUT_ARGUMENT_ERROR.getCode().toString())
                .setExceptionCode(ExceptionCode.INPUT_ARGUMENT_ERROR)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(javax.validation.ConstraintViolationException.class)
    protected ResponseEntity<Object> handleJavaXConstraintViolationException(javax.validation.ConstraintViolationException ex) {
        log.error(ExceptionUtils.getStackTrace(ex));
        response.setErrorDetail(new ExceptionModel()
                .setMessage(translator.translate(ex))
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.INPUT_ARGUMENT_ERROR.getCode().toString())
                .setExceptionCode(ExceptionCode.INPUT_ARGUMENT_ERROR)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(DuplicateKeyException.class)
    protected ResponseEntity<Object> handleDuplicateKeyException(DuplicateKeyException ex) {
        log.error(ExceptionUtils.getStackTrace(ex));
        response.setErrorDetail(new ExceptionModel()
                .setMessage(propertiesHandler.getValue("error.repository.DuplicateKeyException"))
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.INPUT_ARGUMENT_ERROR.getCode().toString())
                .setExceptionCode(ExceptionCode.INPUT_ARGUMENT_ERROR)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(IncorrectResultSizeDataAccessException.class)
    protected ResponseEntity<Object> handleIncorrectResultSizeDataAccessException(IncorrectResultSizeDataAccessException ex) {
        log.error(ExceptionUtils.getStackTrace(ex));
        response.setErrorDetail(new ExceptionModel()
                .setMessage(propertiesHandler.getValue("error.repository.incorrectResultSizeDataAccess"))
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.INPUT_ARGUMENT_ERROR.getCode().toString())
                .setExceptionCode(ExceptionCode.INPUT_ARGUMENT_ERROR)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    protected ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex) {
        log.error(ExceptionUtils.getStackTrace(ex));
        response.setErrorDetail(new ExceptionModel()
                .setMessage(propertiesHandler.getValue("error.repository.emptyResult"))
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.INPUT_ARGUMENT_ERROR.getCode().toString())
                .setExceptionCode(ExceptionCode.INPUT_ARGUMENT_ERROR)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex) {
        log.error(ExceptionUtils.getStackTrace(ex));
        response.setErrorDetail(new ExceptionModel()
                .setMessage((ex.getCause() instanceof ConstraintViolationException)
                        ? translator.translate(((ConstraintViolationException) ex.getCause()))
                        : ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.INPUT_ARGUMENT_ERROR.getCode().toString())
                .setExceptionCode(ExceptionCode.INPUT_ARGUMENT_ERROR)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(org.springframework.dao.InvalidDataAccessApiUsageException.class)
    protected ResponseEntity<Object> handleInvalidDataAccessApiUsageException(org.springframework.dao.InvalidDataAccessApiUsageException ex) {
        log.error(ExceptionUtils.getStackTrace(ex));
        response.setErrorDetail(new ExceptionModel()
                .setMessage(propertiesHandler.getValue("error.repository.InvalidDataAccessApiUsageException"))
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.INPUT_ARGUMENT_ERROR.getCode().toString())
                .setExceptionCode(ExceptionCode.INPUT_ARGUMENT_ERROR)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(ObjectOptimisticLockingFailureException.class)
    protected ResponseEntity<Object> handleObjectOptimisticLockingFailureException(ObjectOptimisticLockingFailureException ex) {
        log.error(ExceptionUtils.getStackTrace(ex));
        response.setErrorDetail(new ExceptionModel()
                .setMessage(propertiesHandler.getValue("error.repository.optimisticLocking",
                        ex.getIdentifier(), ex.getMessage()))
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.INPUT_ARGUMENT_ERROR.getCode().toString())
                .setExceptionCode(ExceptionCode.INPUT_ARGUMENT_ERROR)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.NOT_READABLE.getCode().toString())
                .setExceptionCode(ExceptionCode.NOT_READABLE));

        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex,
                                                                     HttpHeaders headers, HttpStatus status,
                                                                     WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.TYPE_NOT_SUPPORTED.getCode().toString())
                .setExceptionCode(ExceptionCode.TYPE_NOT_SUPPORTED)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException ex,
                                                                      HttpHeaders headers, HttpStatus status,
                                                                      WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.TYPE_NOT_ACCEPTABLE.getCode().toString())
                .setExceptionCode(ExceptionCode.TYPE_NOT_ACCEPTABLE)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException ex,
                                                               HttpHeaders headers, HttpStatus status,
                                                               WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.MISSING_PATH_VARIABLE.getCode().toString())
                .setExceptionCode(ExceptionCode.MISSING_PATH_VARIABLE)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
                                                                          HttpHeaders headers, HttpStatus status,
                                                                          WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.MISSING_SERVLET_REQUEST_PARAMETER.getCode().toString())
                .setExceptionCode(ExceptionCode.MISSING_SERVLET_REQUEST_PARAMETER)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex,
                                                                          HttpHeaders headers, HttpStatus status,
                                                                          WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.SERVLET_REQUEST_BINDING_EXCEPTION.getCode().toString())
                .setExceptionCode(ExceptionCode.SERVLET_REQUEST_BINDING_EXCEPTION)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleConversionNotSupported(ConversionNotSupportedException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.CONVERSION_NOT_SUPPORTED.getCode().toString())
                .setExceptionCode(ExceptionCode.CONVERSION_NOT_SUPPORTED)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
                                                        HttpStatus status, WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.TYPE_MIS_MATCH.getCode().toString())
                .setExceptionCode(ExceptionCode.TYPE_MIS_MATCH)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.HTTP_MESSAGE_NOT_WRITABLE.getCode().toString())
                .setExceptionCode(ExceptionCode.HTTP_MESSAGE_NOT_WRITABLE)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(propertiesHandler.getValue(
                        ex.getBindingResult().getFieldError().getDefaultMessage(),
                        propertiesHandler.getValue(ex.getBindingResult().getFieldError().getObjectName()
                                + "." + ex.getBindingResult().getFieldError().getField())
                ))
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.METHOD_ARGUMENT_NOT_VALID.getCode().toString())
                .setExceptionCode(ExceptionCode.METHOD_ARGUMENT_NOT_VALID)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex,
                                                                     HttpHeaders headers, HttpStatus status,
                                                                     WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.MISSING_SERVLET_REQUEST_PART.getCode().toString())
                .setExceptionCode(ExceptionCode.MISSING_SERVLET_REQUEST_PART)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers,
                                                         HttpStatus status, WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.BIND_EXCEPTION.getCode().toString())
                .setExceptionCode(ExceptionCode.BIND_EXCEPTION)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
                                                                   HttpStatus status, WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.NO_HANDLER_FOUND_EXCEPTION.getCode().toString())
                .setExceptionCode(ExceptionCode.NO_HANDLER_FOUND_EXCEPTION)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleAsyncRequestTimeoutException(AsyncRequestTimeoutException ex,
                                                                        HttpHeaders headers, HttpStatus status,
                                                                        WebRequest webRequest) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(webRequest);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.ASYNC_REQUEST_TIMEOUT_EXCEPTION.getCode().toString())
                .setExceptionCode(ExceptionCode.ASYNC_REQUEST_TIMEOUT_EXCEPTION)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
                                                             HttpStatus status, WebRequest request) {
        log.error(ExceptionUtils.getStackTrace(ex));
        HttpBaseException exception = new HttpBaseException(ex.getMessage());
        exception.setHeaders(headers);
        exception.setRequest(request);
        exception.setHttpMessage(ex.getMessage());
        exception.setStatus(status);
        response.setErrorDetail(new ExceptionModel()
                .setMessage(ex.getMessage())
                .setStackTrace(stackTraceHandler.handle(ex))
                .setCode(ExceptionCode.EXCEPTION_INTERNAL.getCode().toString())
                .setExceptionCode(ExceptionCode.EXCEPTION_INTERNAL)
        );
        response.setResponseCode(ResponseCode.EXCEPTION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


//    @Override
//    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
//                                                                         HttpHeaders headers, HttpStatus status,
//                                                                         WebRequest request) {
//        ExceptionHttpBase exception = new ExceptionHttpBase(ex.getMessage());
//        exception.setHeaders(headers);
//        exception.setRequest(request);
//        exception.setHttpMessage(ex.getMessage());
//        exception.setStatus(status);
//        response.setErrorDetail(new ExceptionModel()
//               .setMessage(ex.getMessage())
//                .setStackTrace(stackTraceHandler.handle(ex))
//                .setCode(ExceptionCode.METHOD_NOT_SUPPORTED.getCode().toString())
//                .setExceptionCode(ExceptionCode.METHOD_NOT_SUPPORTED)
//                );
//        response.setResponseCode(ResponseCode.EXCEPTION);
//        return new ResponseEntity<>(response, HttpStatus.OK);
//    }

}
