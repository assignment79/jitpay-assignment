package org.garm.infrastructure.base.domain;

import javax.persistence.Column;
import javax.persistence.Transient;

public interface LogicalDeleted {

    @Column(name = "DELETED", nullable = false)
    boolean isDeleted();

    void setDeleted(boolean deleted);

    @Transient
    default void toggleDeleted() {
        setDeleted(!isDeleted());
    }

}
