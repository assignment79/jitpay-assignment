package org.garm.infrastructure.base.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.PathPatternsRequestCondition;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.annotation.PostConstruct;
import java.util.Map;

@Component
public class ModuleAction {

    @Autowired
    private RequestMappingHandlerMapping handlerMapping;
    private static RequestMappingHandlerMapping handler;

    @PostConstruct
    public void init() {
        handler = handlerMapping;
    }

    public static void print() {
        Map<RequestMappingInfo, HandlerMethod> handlerMethods = handler.getHandlerMethods();

        String format = "INSERT INTO um_action (id, version, enabled, name, title, url, system_id) values (nextval('um_action_seq'), 0, true, '%s', '%s', '%s', 1);%n";
        handlerMethods.keySet().forEach(requestMappingInfo -> {
            String url;
            PatternsRequestCondition patternsCondition = requestMappingInfo.getPatternsCondition();
            if (patternsCondition == null) {
                PathPatternsRequestCondition pathPatternsCondition = requestMappingInfo.getPathPatternsCondition();
                assert pathPatternsCondition != null;
                url = pathPatternsCondition.getPatterns().toString();
            } else {
                url = patternsCondition.getPatterns().toString();
            }
            url = url.replace("]", "");
            url = url.replace("[", "");
            System.out.printf(format, url, url, url);
//                System.out.println("/api/pfi" + url);

        });
        System.out.println();
    }

}
