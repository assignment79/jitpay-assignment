package org.garm.infrastructure.base.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.SneakyThrows;
import org.springframework.boot.jackson.JsonComponent;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.TimeZone;

//@JsonComponent
public class LocalDateDeserializerISO extends JsonDeserializer<LocalDate> {

    @SneakyThrows
    @Override
    public LocalDate deserialize(JsonParser parser, DeserializationContext context) {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX")
                .parse(parser.readValueAs(String.class))
                .toInstant()
                .atZone(TimeZone.getTimeZone("GMT").toZoneId())
                .toLocalDate();
    }

}