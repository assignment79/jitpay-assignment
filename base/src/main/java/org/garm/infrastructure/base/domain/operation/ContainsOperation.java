package org.garm.infrastructure.base.domain.operation;

import org.garm.infrastructure.base.dto.Filter;
import org.garm.infrastructure.base.utils.SpecialCharacter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class ContainsOperation<E> extends AbstractOperator<E> {

    public ContainsOperation(Root<E> root, CriteriaBuilder criteriaBuilder) {
        super(root, criteriaBuilder);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Predicate execute(Filter filter) {
        return criteriaBuilder.like(criteriaBuilder.lower(relationalGetPath(root, filter.getFieldName())),
                SpecialCharacter.PERCENT + filter.getValue().toString().toLowerCase() + SpecialCharacter.PERCENT);
    }
}

