package org.garm.infrastructure.base.cache.local;


public interface LocalCacheHandler<T> {
    T get(String key, Class objectClass);

    T put(String key, T object, Class objectClass);

    T put(String key, T object, Class objectClass, long timeout);

    void remove(String key);


}
