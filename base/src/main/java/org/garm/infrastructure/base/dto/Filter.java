package org.garm.infrastructure.base.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.garm.infrastructure.base.enums.FilterOperation;
import org.garm.infrastructure.base.model.service.view.InfBaseView;
import org.garm.infrastructure.base.utils.LocalDeserializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Filter {

    @NotNull(message = "error.validation.notNull")
    @JsonView({InfBaseView.ResponseView.class})
    private String fieldName;

    @NotNull(message = "error.validation.notNull")
    @JsonView({InfBaseView.ResponseView.class})
    private FilterOperation operation;

    @NotNull(message = "error.validation.notNull")
    @JsonView({InfBaseView.ResponseView.class})
    @JsonDeserialize(using = LocalDeserializer.class)
    private Object value;
}
