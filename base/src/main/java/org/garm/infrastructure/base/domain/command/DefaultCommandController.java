package org.garm.infrastructure.base.domain.command;

import org.garm.infrastructure.base.exception.NoSuchServiceExistException;
import org.garm.infrastructure.base.model.general.BaseDto;
import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.base.service.obj.VoidObject;

import java.io.Serializable;

public interface DefaultCommandController<D extends BaseDto, L extends Serializable> {

    default Response<D, L> create(D dto) {
        throw new NoSuchServiceExistException();
    }

    default Response<L, Void> delete(L id) {
        throw new NoSuchServiceExistException();
    }

    default Response<D, Void> update(D dto) {
        throw new NoSuchServiceExistException();
    }

}

