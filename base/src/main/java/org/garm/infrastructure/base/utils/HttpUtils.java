package org.garm.infrastructure.base.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.garm.infrastructure.base.utils.StringUtils.mapToString;


public final class HttpUtils {

    private HttpUtils() {
    }

    public static <T> HttpEntity<T> createPostHeader(T body) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Stream.of(MediaType.APPLICATION_JSON).collect(Collectors.toList()));
        return new HttpEntity<>(body, headers);
    }

    public static HttpEntity<Object> createGetHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Stream.of(MediaType.APPLICATION_JSON).collect(Collectors.toList()));
        return new HttpEntity<>(headers);
    }

    public static HttpEntity<Object> createGetHeader(String logToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Stream.of(MediaType.APPLICATION_JSON).collect(Collectors.toList()));
        headers.add("log-token", logToken);
        return new HttpEntity<>(headers);
    }

    public static void serRequestFactory(RestTemplate restTemplate) {
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
    }

    public static <T> HttpEntity<T> createHeader(T body, HttpMethod httpMethodType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Stream.of(MediaType.APPLICATION_JSON).collect(Collectors.toList()));
        headers.setAllow(new HashSet<HttpMethod>() {
            {
                add(httpMethodType);
            }
        });

        return Objects.isNull(body) ? new HttpEntity<>(headers) : new HttpEntity<>(body, headers);
    }

    public static String generateUrl(String base, Map<String, String> parameters) {
        return base + mapToString(parameters, "&", "=", "?", "");
    }

    public static boolean equalsAcceptEncoding(HttpServletRequest request, String encoding) {
        String requestEncoding = request.getHeader("accept-encoding");
        return (requestEncoding != null && requestEncoding.equals(encoding)) ? true : false;
    }

    public static HttpHeaders createFileHeader(String fileName, String contentType, long contentSize) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentLength(contentSize);
        String[] contentTypes = contentType.split("/", 2);
        httpHeaders.setContentType(new MediaType(contentTypes[0], contentTypes[1]));
        httpHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        httpHeaders.set(HttpHeaders.CONTENT_DISPOSITION,
                String.format("attachment; filename=%s%s%s", fileName, ".", contentTypes[1]));
        return httpHeaders;
    }
}

