package org.garm.infrastructure.base.enums;

public interface Convertible<T> {
    T getValue();
}
