package org.garm.infrastructure.base.domain.query;

import org.garm.infrastructure.base.domain.AbstractPersistence;
import org.garm.infrastructure.base.domain.pagination.PaginationExecutor;
import org.garm.infrastructure.base.domain.repository.BaseRepository;
import org.garm.infrastructure.base.dto.PagingRequest;
import org.garm.infrastructure.base.dto.PagingResponse;
import org.garm.infrastructure.base.exception.EntityNotFoundException;
import org.garm.infrastructure.base.mapper.BaseMapper;
import org.garm.infrastructure.base.model.general.BaseDto;

import java.io.Serializable;
import java.util.List;

public interface DefaultQueryService<E extends AbstractPersistence<L>, D extends BaseDto<L>, L extends Serializable> {

    default BaseRepository<E, L> getRepository() {
        throw new UnsupportedOperationException();
    }

    default BaseMapper<E, D, L> getMapper() {
        throw new UnsupportedOperationException();
    }

    default D findById(L id) {

        return getMapper().map(getRepository().findById(id).orElseThrow(() -> new EntityNotFoundException("error.entity.not.found", id)));
    }

    default PagingResponse<List<D>> findPaging(PagingRequest pagingRequest) {

        return (new PaginationExecutor<>(pagingRequest, getRepository(), getMapper())).execute();
    }
}
