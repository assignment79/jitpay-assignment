package org.garm.infrastructure.base.utils;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

public class DownloadUtils {

    public static ResponseEntity<Resource> download(String filename, byte[] content) {

        return download(filename, content, new HttpHeaders());
    }

    public static ResponseEntity<Resource> download(String filename, byte[] content, HttpHeaders header) {
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + filename);
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Content-Type", "application/octet-stream");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");

        ByteArrayResource resource = new ByteArrayResource(content);

        return ResponseEntity.ok()
                .headers(header)
                .contentLength(content.length)
                .body(resource);
    }

}
