package org.garm.infrastructure.base.exception;

import org.garm.infrastructure.base.enums.ExceptionCode;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class ExceptionModel {
    private String message;
    private Object stackTrace;
    private String code;
    private ExceptionCode exceptionCode;
}
