package org.garm.infrastructure.base.exception;

import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class StackTraceHandler {

    private final Environment env;

    public String handle(Exception e) {
        String stackTrace = Strings.EMPTY;
        if (Arrays.stream(env.getActiveProfiles()).noneMatch(profile -> profile.contains("prod"))) {
            stackTrace = Arrays.stream(e.getStackTrace()).collect(Collectors.toList())
                    .stream()
                    .filter(stackTraceElement -> stackTraceElement.getClassName().startsWith("org.garm."))
                    .map(stackTraceElement -> stackTraceElement.getClassName() + (".")
                            + (stackTraceElement.getMethodName()) + (" : line [")
                            + (stackTraceElement.getLineNumber()) + "]")
                    .collect(Collectors.joining(" | "));
        }
        return stackTrace;
    }

}
