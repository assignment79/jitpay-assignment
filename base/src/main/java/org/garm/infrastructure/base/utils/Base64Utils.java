package org.garm.infrastructure.base.utils;

import java.util.Base64;
import java.util.Objects;

public final class Base64Utils {
    public static byte[] getFilePartOf(String base64FileString) {
        return Objects.isNull(base64FileString) ? null : Base64.getDecoder().decode(base64FileString.split(",")[1]);
    }

    public static String getMimeType(String base64FileString) {
        return Objects.isNull(base64FileString) ? null : base64FileString.split(";")[0].replace("data:", "");
    }

    public static String makeWithMimeAndByteArray(String mimeType, byte[] src) {
        return Objects.isNull(src) ? null : "data:" + mimeType + ";base64," + Base64.getEncoder().encodeToString(src);
    }
}
