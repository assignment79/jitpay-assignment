package org.garm.infrastructure.base.exception;

import org.garm.infrastructure.base.enums.ExceptionCode;

public class EntityNotFoundException extends BaseException {

    public EntityNotFoundException(String message, Object ...args) {
        super(message);
        setArguments(args);
        setExceptionCode(ExceptionCode.NOT_FOUND);
    }
}
