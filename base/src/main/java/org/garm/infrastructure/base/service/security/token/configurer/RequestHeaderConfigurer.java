package org.garm.infrastructure.base.service.security.token.configurer;

import feign.RequestTemplate;

import javax.servlet.http.HttpServletRequest;

public interface RequestHeaderConfigurer {

    default RequestHeaderConfigurer next() {
        return null;
    }

    boolean config(HttpServletRequest request, RequestTemplate requestTemplate);
}