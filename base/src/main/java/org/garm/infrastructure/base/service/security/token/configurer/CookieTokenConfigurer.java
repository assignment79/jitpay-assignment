package org.garm.infrastructure.base.service.security.token.configurer;

import feign.RequestTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CookieTokenConfigurer implements RequestHeaderConfigurer {

    @Override
    public RequestHeaderConfigurer next() {
        return new BearerTokenConfigurer();
    }

    @Override
    public boolean config(HttpServletRequest request, RequestTemplate requestTemplate) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                requestTemplate.header("Cookie", cookie.getName() + "=" + cookie.getValue());
            }
            return true;
        }
        return false;
    }
}