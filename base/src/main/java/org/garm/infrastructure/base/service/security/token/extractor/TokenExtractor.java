package org.garm.infrastructure.base.service.security.token.extractor;

import lombok.Getter;
import lombok.Setter;

import javax.servlet.http.HttpServletRequest;

@Getter
@Setter
public class TokenExtractor implements Extractor {

    private Extractor extractor;

    @Override
    public String extract() {
        return this.extractor.extract();
    }
}