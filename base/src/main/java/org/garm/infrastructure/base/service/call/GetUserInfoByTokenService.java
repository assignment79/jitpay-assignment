package org.garm.infrastructure.base.service.call;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.garm.infrastructure.base.model.security.AuthDto;
import org.garm.infrastructure.base.model.security.CurrentUserInfoDto;
import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.base.utils.DefaultFallBackBuilder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(url = "${user.management.base.url}",
        contextId = "SECURITY-USER-INFO",
        value = "USER-MANAGEMENT")
public interface GetUserInfoByTokenService {

    @CircuitBreaker(name = "get-current-user", fallbackMethod = "doFallBack")
    @PostMapping("/facade/users/get-current-user")
    Response<AuthDto, CurrentUserInfoDto> apply(@RequestBody AuthDto auth);

    default Response<AuthDto, CurrentUserInfoDto> doFallBack(AuthDto auth, Throwable throwable) {
        return DefaultFallBackBuilder.build(auth, throwable);
    }
}