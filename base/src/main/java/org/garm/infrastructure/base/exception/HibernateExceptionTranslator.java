package org.garm.infrastructure.base.exception;

import org.garm.infrastructure.base.config.PropertiesHandler;
import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import java.beans.Introspector;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class HibernateExceptionTranslator {

    private final MessageSource messageSource;
    private final PropertiesHandler propertiesHandler = new PropertiesHandler();

    public String translate(ConstraintViolationException ex) {

        switch (ex.getSQLState()) {

            case "23503":
                return propertiesHandler.getValue("error.repository.foreignKeyViolation",
                        propertiesHandler.getValue(ex.getConstraintName()));
            case "23505":
                return propertiesHandler.getValue("repository.uniqueViolation",
                        propertiesHandler.getValue(ex.getConstraintName()));
            case "23502":
                return propertiesHandler.getValue("error.repository.notNullViolation",
                        propertiesHandler.getValue(ex.getConstraintName()));
            default:
                return propertiesHandler.getValue("error.orm.general",
                        ex.getSQLException().getErrorCode());

        }
    }

    public String translate(javax.validation.ConstraintViolationException ex) {

        List<String> messages = new ArrayList<>();
        for (ConstraintViolation<?> constraint : ex.getConstraintViolations()) {
            String annotationType = constraint.getConstraintDescriptor().getAnnotation().annotationType().getName();
            String messagePath = Introspector.decapitalize(Hibernate.getClass(constraint.getLeafBean()).getSimpleName()) + "." + constraint.getPropertyPath();
            switch (annotationType) {
                case "javax.validation.constraints.NotNull":
                case "org.hibernate.validator.constraints.NotEmpty":
                    messages.add(messageSource.getMessage(messagePath + ".empty", null, LocaleContextHolder.getLocale()));
                    break;
                case "org.hibernate.validator.constraints.Length":
                case "javax.validation.constraints.Size":
                    messages.add(messageSource.getMessage(messagePath + ".length",
                            new Object[]{constraint.getConstraintDescriptor().getAttributes().get("max")}, LocaleContextHolder.getLocale()));
                    break;
            }
        }

        return String.join("\n", messages);
    }

    //    public static String translate(ConstraintViolationException ex) {
    //
    //        switch (ex.getSQLException().getErrorCode()) {
    //            case 1:
    //                return propertiesHandler.getValue("error.orm.01",
    //                        propertiesHandler.getValue(ex.getConstraintName()));
    //            case 2292:
    //                return propertiesHandler.getValue("error.orm.02292",
    //                        propertiesHandler.getValue(ex.getConstraintName()));
    //            default:
    //                return propertiesHandler.getValue("error.orm.general",
    //                        ex.getSQLException().getErrorCode());
    //        }
    //    }
}
