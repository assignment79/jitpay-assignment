package org.garm.infrastructure.base.dto;

import com.fasterxml.jackson.annotation.JsonView;
import org.garm.infrastructure.base.model.service.view.InfBaseView;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PagingRequest {

    @NotNull(message = "error.validation.notNull")
    @JsonView({InfBaseView.ResponseView.class})
    private Integer start;
    @NotNull(message = "error.validation.notNull")
    @Min(value = 1, message = "error.validation.min")
    @JsonView({InfBaseView.ResponseView.class})
    private Integer size;
    @Valid
    @NotNull(message = "error.validation.notNull")
    @JsonView({InfBaseView.ResponseView.class})
    private List<Filter> filters;
    @JsonView({InfBaseView.ResponseView.class})
    private PagingSort sort;

}
