package org.garm.infrastructure.base.validator.mobilenumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class MobileNumberValidator implements ConstraintValidator<MobileNumber, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (Objects.isNull(value)) {
            return true;
        }
        return mobileNumberEvaluation(value);
    }

    private boolean mobileNumberEvaluation(String value) {
        return value.matches("^[0]\\d{10}$");
    }
}
