package org.garm.infrastructure.base.cache;

import org.garm.infrastructure.base.cache.local.LocalCacheHandlerImpl;
import org.garm.infrastructure.base.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.redis.core.RedisTemplate;

@Slf4j
public class CacheHandler<T> extends LocalCacheHandlerImpl<T> {

    private final RedisTemplate<String, Object> redisTemplate;
    private final String prefix;
    private final Class<T> objectClass;

    public CacheHandler(String prefix, RedisTemplate<String, Object> redisTemplate, Class<T> objectClass) {
        super(prefix);
        this.redisTemplate = redisTemplate;
        this.objectClass = objectClass;
        this.prefix = prefix;
    }

    public T put(String key, T object) {
        super.put(prefix + key, object, objectClass);
        log.info("saved into cache -> " + object);
        return object;
    }

    public T put(String key, T object, long timeout) {
        super.put(prefix + key, object, objectClass);
        RedisUtils.resetExpire(prefix + key, timeout);
        log.info("saved into cache -> " + object);
        return object;
    }

    public T get(String key) {
        T object = super.get(prefix + key, objectClass);
        log.info("fetch from cache -> " + object);
        return object;
    }

    public T take(String key) {
        T object = get(key, objectClass);
        delete(key);
        log.info("take from cache -> " + object);
        return object;
    }

    public void delete(String key) {
        redisTemplate.delete(key);
    }
}
