package org.garm.infrastructure.base.model;

import java.util.Date;
import java.util.UUID;

public interface EventBase {

    UUID getEventId();

    Date getDate();

}