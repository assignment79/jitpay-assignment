package org.garm.infrastructure.base.service.security.token.configurer;

import feign.RequestTemplate;
import org.garm.infrastructure.base.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Objects;

import static org.garm.infrastructure.base.utils.SpecialCharacter.Security.AUTHORIZATION;

public class BearerTokenConfigurer implements RequestHeaderConfigurer {

    @Override
    public boolean config(HttpServletRequest request, RequestTemplate requestTemplate) {
        Enumeration<String> headerNames = request.getHeaderNames();
        if (Objects.nonNull(headerNames) && headerNames.hasMoreElements()) {
            while (headerNames.hasMoreElements()) {
                String element = headerNames.nextElement();
                if (StringUtils.lowerCamelCaseUnderscoreSplit(element).equals(AUTHORIZATION)) {
                    requestTemplate.header(AUTHORIZATION, request.getHeader(element));
                    break;
                }
            }
        }
        return true;
    }
}