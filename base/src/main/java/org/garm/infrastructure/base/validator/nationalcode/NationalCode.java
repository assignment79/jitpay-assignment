package org.garm.infrastructure.base.validator.nationalcode;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = NationalCodeValidator.class)
@Target({FIELD})
@Retention(RUNTIME)
public @interface NationalCode {
    String value() default "";

    String message() default "{error.validation.national.code}";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
