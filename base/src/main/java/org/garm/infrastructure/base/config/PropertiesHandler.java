package org.garm.infrastructure.base.config;

import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.Properties;


@Component
public class PropertiesHandler {

    private final LinkedList<String> resources = new LinkedList<>();
    private static final String EXTERNAL_PATH = "/root/config";

    public PropertiesHandler() {
        resources.add("/messages_en.properties");
        resources.add("/local_messages_fa.properties");
        resources.add(EXTERNAL_PATH + "/messages_en.properties");
        resources.add(EXTERNAL_PATH + "/local_messages_fa.properties");
    }

    public String getValue(String key, Object... arguments) {
        String ans = null;
        for (String resource : resources) {
            ans = this.getPropValue(resource, key, arguments);
            if (ans != null) break;
        }
        return ans == null ? key : ans;
    }

    public String getPropValue(String resource, String key, Object... arguments) {
        Properties properties = new Properties();
        try {
            if (resource.startsWith(EXTERNAL_PATH)) {
                properties.load(new FileInputStream(new File(resource)));
            } else {
                properties.load(PropertiesHandler.class.getResourceAsStream(resource));
            }
        } catch (Exception e) {
            return null;
        }

        String value = properties.getProperty(key);
        if (value == null || arguments == null) return null;
        for (
                int i = 0;
                i < arguments.length; i++) {
            value = value.replace(String.format("{%d}", i), String.valueOf(arguments[i]));
        }
        return value;
    }
}