package org.garm.infrastructure.base.utils;

import org.garm.infrastructure.base.enums.ResponseCode;
import org.garm.infrastructure.base.exception.ExceptionModel;
import org.garm.infrastructure.base.model.service.Response;

public class DefaultFallBackBuilder {
    public static <I, O> Response<I, O> build(I in, Throwable throwable) {
        Response<I, O> response = new Response<>();
        response.setInputArguments(in);
        response.setResponseCode(ResponseCode.EXCEPTION);
        response.setErrorDetail(new ExceptionModel().setMessage(throwable.getMessage()));
        return response;
    }
}