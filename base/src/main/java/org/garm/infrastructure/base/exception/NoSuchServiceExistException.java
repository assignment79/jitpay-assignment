package org.garm.infrastructure.base.exception;

import org.garm.infrastructure.base.enums.ExceptionCode;

public class NoSuchServiceExistException extends BaseException {

    public NoSuchServiceExistException() {
        super("error.rest.notSupported");
        setExceptionCode(ExceptionCode.METHOD_NOT_SUPPORTED);
    }
}
