package org.garm.infrastructure.base.dto;

import com.fasterxml.jackson.annotation.JsonView;
import org.garm.infrastructure.base.enums.SortOperation;
import org.garm.infrastructure.base.model.service.view.InfBaseView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PagingSort {
    @JsonView({InfBaseView.ResponseView.class})
    private String[] multiField;
    @JsonView({InfBaseView.ResponseView.class})
    private String fieldName;
    @JsonView({InfBaseView.ResponseView.class})
    private SortOperation operation;
}
