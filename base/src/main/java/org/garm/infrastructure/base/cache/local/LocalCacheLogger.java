
package org.garm.infrastructure.base.cache.local;

import org.garm.infrastructure.base.logger.Logger;
import org.springframework.stereotype.Component;

@Component
public class LocalCacheLogger extends Logger {
    @Override
    public String getTagName() {
        return "Local-Cache-Logger";
    }
}
