package org.garm.infrastructure.base.model.security;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AuthDto {
    private String clientId;
    private String username;
}
