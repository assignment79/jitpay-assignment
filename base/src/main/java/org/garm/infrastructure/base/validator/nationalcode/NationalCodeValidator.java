package org.garm.infrastructure.base.validator.nationalcode;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;
import java.util.stream.IntStream;

public class NationalCodeValidator implements ConstraintValidator<NationalCode, String> {
    public static boolean NationalCodeEvaluation(String code) {
        if (!code.matches("^\\d{10}$"))
            return false;
        int check = Integer.parseInt(code.substring(9, 10));
        int sum = IntStream.range(0, 9)
                .map(x -> Integer.parseInt(code.substring(x, x + 1)) * (10 - x))
                .sum() % 11;
        return (sum < 2 && check == sum) || (sum >= 2 && check + sum == 11);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (Objects.isNull(value)) {
            return true;
        }
        return NationalCodeEvaluation(value);
    }
}
