package org.garm.infrastructure.base.service.security.token.extractor;

import org.garm.infrastructure.base.utils.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import java.util.Arrays;

import static org.garm.infrastructure.base.utils.SecurityUtils.TOKEN_KEY;
import static org.garm.infrastructure.base.utils.SecurityUtils.extractToken;

public class CookieAccessTokenExtractor implements Extractor {

    @Override
    public Extractor next() {
        return new BearerAccessTokenExtractor();
    }

    @Override
    public String extract() {
        String token = StringUtils.EMPTY;
        ServletRequestAttributes request = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (request != null) {
            if (ArrayUtils.isNotEmpty(request.getRequest().getCookies())) {
                Cookie accessToken = Arrays.stream(request.getRequest().getCookies())
                        .filter(cookie -> cookie.getName().equals(TOKEN_KEY))
                        .findFirst().orElse(null);
                token = accessToken != null ? extractToken(accessToken.getValue()) : StringUtils.EMPTY;
            }
        }
        return token;
    }


}