package org.garm.infrastructure.base.service.call;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.base.service.obj.SmsDto;
import org.garm.infrastructure.base.service.obj.VoidObject;
import org.garm.infrastructure.base.utils.DefaultFallBackBuilder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(url = "${service.gateway.send.sms}",
        contextId = "GATEWAY-PUT-SMS-SENDER",
        value = "INFRASTRUCTURE-SERVICE-GATEWAY")
public interface SmsSenderService {

    @CircuitBreaker(name = "send-sms", fallbackMethod = "doFallBack")
    @PutMapping("/send/{subsystem}")
    Response<List<SmsDto>, Void> apply(@PathVariable String subsystem, @RequestBody List<SmsDto> dtoList);

    default Response<List<SmsDto>, VoidObject> doFallBack(String subsystem, List<SmsDto> dtoList, Throwable throwable) {
        return DefaultFallBackBuilder.build(dtoList, throwable);
    }
}
