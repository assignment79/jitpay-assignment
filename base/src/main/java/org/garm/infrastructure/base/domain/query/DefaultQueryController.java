package org.garm.infrastructure.base.domain.query;

import org.garm.infrastructure.base.dto.PagingRequest;
import org.garm.infrastructure.base.dto.PagingResponse;
import org.garm.infrastructure.base.model.general.BaseDto;
import org.garm.infrastructure.base.model.service.Response;

import java.io.Serializable;

public interface DefaultQueryController<D extends BaseDto, L extends Serializable> {

    default Response<L, D> findById(L id) {
        return null;
    }

    default Response<PagingRequest, PagingResponse> findPaging(PagingRequest pagingRequest) {
        return null;
    }

}
