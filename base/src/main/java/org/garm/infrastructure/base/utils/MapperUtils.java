
package org.garm.infrastructure.base.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.garm.infrastructure.base.domain.AbstractPersistence;
import org.garm.infrastructure.base.domain.repository.BaseRepository;
import org.garm.infrastructure.base.exception.EntityNotFoundException;
import org.garm.infrastructure.base.exception.NotSameVersionException;
import org.garm.infrastructure.base.model.general.BaseDto;
import lombok.SneakyThrows;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Objects;

public class MapperUtils {

    @SneakyThrows
    public static byte[] encodeToBase64(MultipartFile multipartFile) {
        return Objects.nonNull(multipartFile) ? java.util.Base64.getEncoder().encode(multipartFile.getBytes()): null;
    }

    @SneakyThrows
    public static <T> T fromJsonToJavaObject(String json, Class<T> classOfT) {
        return Objects.nonNull(json)? new ObjectMapper().readValue(json, classOfT): null;
    }

    @SneakyThrows
    public static <T> String javaObjectToJson(T object) {
        return new ObjectMapper().writeValueAsString(object);
    }
    @SuppressWarnings("unchecked")
    public static <R extends BaseRepository<E, L>, E extends AbstractPersistence<L>, T extends BaseDto, L extends Serializable> E getEntity(R repository, E entity, T dto) {
        if (Objects.isNull(dto)) {
            return entity;
        }
        if (dto.getId() != null) {
            try {
                entity = repository.findById(((L) dto.getId())).orElse(entity);
            } catch (EntityNotFoundException ex) {
                throw new EntityNotFoundException("error.entity.not.found", dto.getId());
            }
            if (!Objects.equals(dto.getVersion(), entity.getVersion())) {
                throw new NotSameVersionException();
            }
        }
        return entity;
    }
}
