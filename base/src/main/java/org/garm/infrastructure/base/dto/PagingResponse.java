package org.garm.infrastructure.base.dto;

import com.fasterxml.jackson.annotation.JsonView;
import org.garm.infrastructure.base.model.service.view.InfBaseView;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class PagingResponse<D> {

    @JsonView({InfBaseView.ResponseView.class})
    private int start;
    @JsonView({InfBaseView.ResponseView.class})
    private int size;
    @JsonView({InfBaseView.ResponseView.class})
    private int count;
    @JsonView({InfBaseView.ResponseView.class})
    private D data;

}
