package org.garm.infrastructure.base.utils;

public interface SpecialCharacter {
    String DASH = "-";
    String DOT = ".";
    String PERCENT = "%";
    String SLASH = "/";
    String BACK_SLASH = "\\";
    String BACK_DOT = "\\.";
    String EQUALS = "=";
    String QUESTION_MARK = "?";
    String AND = "&";
    String SPACE = " ";
    String UNDER_LINE = "_";

    interface Security {
        String AUTHORIZATION = "authorization";
    }
}
