package org.garm.infrastructure.base.cache.local;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.garm.infrastructure.base.cache.local.repository.RedisRepository;
import org.garm.infrastructure.base.utils.RedisUtils;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class LocalCacheHandlerImpl<T> implements LocalCacheHandler<T> {

    private final String prefix;

    public LocalCacheHandlerImpl() {
        prefix = "";
    }

    public LocalCacheHandlerImpl(String prefix) {
        this.prefix = prefix;
    }

    @Autowired
    private RedisRepository redisRepository;

    @Autowired
    private LocalCacheLogger localCacheLogger;

    @Override
    public T get(String key, Class objectClass) {
        T ans = null;
        try {
            LocalCache localCache = this.redisRepository.findByMetadata(key);
            ans = (T) new ObjectMapper().readValue(localCache.getJsonObject(), objectClass);
        } catch (Exception e) {
            localCacheLogger.info("this key is not available in local cache.");
        }
        return ans;
    }

    @SneakyThrows
    @Override
    public T put(String key, T object, Class objectClass) {

        if (this.get(key, objectClass) == null) {
            this.redisRepository.save(LocalCache.builder()
                    .metadata(key)
                    .jsonObject(new ObjectMapper().writeValueAsString(object))
                    .build());
            return object;
        } else {
            LocalCache localCache = this.redisRepository.findByMetadata(key);
            this.redisRepository.delete(localCache);
            return this.put(key, object, objectClass);
        }
    }

    @Override
    public void remove(String key) {
        LocalCache localCache = this.redisRepository.findByMetadata(key);
        this.redisRepository.delete(localCache);
    }

    @Override
    public T put(String key, T object, Class objectClass, long timeout) {
        T out = this.put(key, object, objectClass);
        RedisUtils.resetExpire(key, timeout);
        return out;
    }

//    private String makeKey(String key) {
//        return prefix + key;
//    }

}
