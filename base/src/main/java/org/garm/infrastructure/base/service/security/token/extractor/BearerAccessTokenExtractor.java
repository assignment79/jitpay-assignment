package org.garm.infrastructure.base.service.security.token.extractor;

import org.garm.infrastructure.base.utils.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Objects;

import static org.garm.infrastructure.base.utils.SecurityUtils.extractToken;
import static org.garm.infrastructure.base.utils.SpecialCharacter.Security.AUTHORIZATION;

public class BearerAccessTokenExtractor implements Extractor {

    @Override
    public String extract() {
        String token = StringUtils.EMPTY;
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attrs != null) {
            HttpServletRequest request = attrs.getRequest();
            Enumeration<String> headerNames = request.getHeaderNames();
            if (Objects.nonNull(headerNames) && headerNames.hasMoreElements()) {
                while (headerNames.hasMoreElements()) {
                    String element = headerNames.nextElement();
                    if (StringUtils.lowerCamelCaseUnderscoreSplit(element).equals(AUTHORIZATION)) {
                        token = extractToken(request.getHeader(element));
                        break;
                    }
                }
            }
        }
        return token;
    }
}