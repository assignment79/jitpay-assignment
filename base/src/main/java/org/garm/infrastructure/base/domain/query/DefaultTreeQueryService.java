package org.garm.infrastructure.base.domain.query;

import org.garm.infrastructure.base.domain.AbstractAuditingTreeEntity;
import org.garm.infrastructure.base.model.general.BaseTreeDto;

import java.io.Serializable;

public interface DefaultTreeQueryService<E extends AbstractAuditingTreeEntity<L, E>, D extends BaseTreeDto<L, D>, L extends Serializable>
        extends DefaultQueryService<E, D, L> {

}
