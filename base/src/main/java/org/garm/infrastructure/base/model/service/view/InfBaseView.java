package org.garm.infrastructure.base.model.service.view;

public class InfBaseView {
    public interface ResponseView {};
    public interface ResponseIdVersionView {};
    public interface AbstractPersistence {};
    public interface AbstractTreePersistence {};
}
