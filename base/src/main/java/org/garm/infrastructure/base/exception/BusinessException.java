package org.garm.infrastructure.base.exception;

import org.garm.infrastructure.base.enums.ExceptionCode;

public final class BusinessException extends BaseException {

    public BusinessException(String message, Object... args) {
        super(message);
        setArguments(args);
        setExceptionCode(ExceptionCode.BUSINESS_EXCEPTION);
    }
}
