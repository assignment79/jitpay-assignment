package org.garm.infrastructure.base.domain.pagination;

import org.garm.infrastructure.base.dto.PagingResponse;
import org.garm.infrastructure.base.dto.PagingSort;
import org.garm.infrastructure.base.model.general.BaseDto;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface Paging<D extends BaseDto> {
    Sort parsingSortRequest(PagingSort sort);

    PagingResponse<List<D>> execute();
}
