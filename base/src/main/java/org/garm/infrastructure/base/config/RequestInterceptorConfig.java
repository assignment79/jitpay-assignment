package org.garm.infrastructure.base.config;

import feign.RequestInterceptor;
import org.garm.infrastructure.base.service.security.token.configurer.CookieTokenConfigurer;
import org.garm.infrastructure.base.service.security.token.configurer.RequestConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;


@Configuration
public class RequestInterceptorConfig {

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if (attrs != null) {
                HttpServletRequest request = attrs.getRequest();

                RequestConfigurer requestConfigurer = new RequestConfigurer();
                requestConfigurer.setConfigurer(new CookieTokenConfigurer());

                if (!requestConfigurer.config(request, requestTemplate)) {
                    requestConfigurer.getConfigurer().next().config(request, requestTemplate);
                }

            }
        };
    }
}
