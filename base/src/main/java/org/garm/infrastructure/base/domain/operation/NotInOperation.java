package org.garm.infrastructure.base.domain.operation;

import org.garm.infrastructure.base.dto.Filter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class NotInOperation<E> extends AbstractOperator<E> {

    public NotInOperation(Root<E> root, CriteriaBuilder criteriaBuilder) {
        super(root, criteriaBuilder);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Predicate execute(Filter filter) {
        return criteriaBuilder.not(relationalGetPath(root, filter.getFieldName())).in(filter.getValue());
    }
}

