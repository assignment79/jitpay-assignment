package org.garm.infrastructure.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@EnableFeignClients
@SpringBootApplication
@ComponentScan("org.garm")
public class InfrastructureBase {

    public static void main(String[] args) {
        SpringApplication.run(InfrastructureBase.class, args);
    }

}
