package org.garm.infrastructure.base.enums;

public enum ObjectType {
    LONG,
    LIST,
    MAP,
    STRING,
    UNKNOWN;
}
