package org.garm.infrastructure.base.cache.local.repository;

import org.garm.infrastructure.base.cache.local.LocalCache;
import org.springframework.data.repository.CrudRepository;

public interface RedisRepository extends CrudRepository<LocalCache, Long> {

    LocalCache findByMetadata(String metadata);

    LocalCache findById(long id);

}
