package org.garm.infrastructure.base.model.general;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.garm.infrastructure.base.model.service.view.InfBaseView;
import org.garm.infrastructure.base.utils.LocalDateTimeDeserializer;
import org.garm.infrastructure.base.utils.LocalDateTimeSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
public class AuditingDto<I extends Serializable> extends BaseDto<I> {
    @JsonView(InfBaseView.ResponseView.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    protected LocalDateTime insertDate;
    @JsonView(InfBaseView.ResponseView.class)
    protected Long insertUserId;
    @JsonView(InfBaseView.ResponseView.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    protected LocalDateTime modifyDate;
    @JsonView(InfBaseView.ResponseView.class)
    protected Long modifyUserId;
}
