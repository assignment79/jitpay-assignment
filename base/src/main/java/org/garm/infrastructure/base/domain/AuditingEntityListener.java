package org.garm.infrastructure.base.domain;

import org.garm.infrastructure.base.utils.DateUtils;
import org.garm.infrastructure.base.utils.SecurityUtils;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class AuditingEntityListener {

    /**
     * before insert records
     */
    @PrePersist
    public void touchForCreate(Object target) {
        AbstractPersistence entity = AbstractPersistence.class.cast(target);
        entity.setInsertUserId(SecurityUtils.getCurrentUserId());
        entity.setInsertDate(DateUtils.getCurrentLocalDateTime());
    }

    /**
     * before update records
     */
    @PreUpdate
    public void touchForUpdate(Object target) {
        AbstractPersistence entity = AbstractPersistence.class.cast(target);
        entity.setModifyUserId(SecurityUtils.getCurrentUserId());
        entity.setModifyDate(DateUtils.getCurrentLocalDateTime());
    }
}