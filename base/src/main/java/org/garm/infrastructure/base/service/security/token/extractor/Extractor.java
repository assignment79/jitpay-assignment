package org.garm.infrastructure.base.service.security.token.extractor;

import javax.servlet.http.HttpServletRequest;

public interface Extractor {

    default Extractor next() {
        return null;
    }

    default String extract(HttpServletRequest request){return null;};

    default String extract() {
        return null;
    }
}