package org.garm.infrastructure.base.exception;

import org.garm.infrastructure.base.enums.ExceptionCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceException extends RuntimeException {

    private String code;
    private String message;
    private Object[] arguments;
    private ExceptionCode exceptionCode;

    public ServiceException(String message) {
        super(message);
        this.message = message;
    }

    public ServiceException(String message, boolean isInfrastructureMessage) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
