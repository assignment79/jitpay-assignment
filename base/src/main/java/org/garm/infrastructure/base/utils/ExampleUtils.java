package org.garm.infrastructure.base.utils;

import org.garm.infrastructure.base.domain.AbstractPersistence;
import org.garm.infrastructure.base.domain.repository.BaseRepository;
import org.garm.infrastructure.base.exception.EntityNotFoundException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;

import java.io.Serializable;

public class ExampleUtils {

    public static <I extends Serializable, E extends AbstractPersistence<I>> boolean exists(BaseRepository<E, I> repository, I id, String field) {
        E entity = repository.findById(id).orElseThrow(() -> new EntityNotFoundException("error.entity.not.found", id));
        return repository.exists(Example.of(entity, ExampleMatcher.matching()
                .withMatcher(field, ExampleMatcher.GenericPropertyMatchers.ignoreCase())));
    }

    public static <I extends Serializable, E extends AbstractPersistence<I>> boolean exists(BaseRepository<E, I> repository, E entity, String field) {
        return repository.exists(Example.of(entity, ExampleMatcher.matching()
                .withMatcher(field, ExampleMatcher.GenericPropertyMatchers.ignoreCase())));
    }

}
