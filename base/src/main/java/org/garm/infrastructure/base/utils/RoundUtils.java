package org.garm.infrastructure.base.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RoundUtils {

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static double round4Digits(double value) {
        final int places = 4;
        return round(value, places);
    }

    public static BigDecimal round(BigDecimal value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal factor =  BigDecimal.valueOf(Math.pow(10, places));
        value = value .multiply(factor);
        BigDecimal tmp = value.setScale(0, RoundingMode.HALF_UP);
        return  tmp.divide(factor);
    }

    public static BigDecimal round4Digits(BigDecimal value) {
        final int places = 4;
        return round(value, places);
    }
}
