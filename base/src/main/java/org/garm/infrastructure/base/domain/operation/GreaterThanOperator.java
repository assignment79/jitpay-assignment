package org.garm.infrastructure.base.domain.operation;

import org.garm.infrastructure.base.dto.Filter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;

public class GreaterThanOperator<E> extends AbstractOperator<E> {

    public GreaterThanOperator(Root<E> root, CriteriaBuilder criteriaBuilder) {
        super(root, criteriaBuilder);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Predicate execute(Filter filter) {
        if (filter.getValue() instanceof Comparable) {
            return criteriaBuilder.greaterThan(relationalGetPath(root, filter.getFieldName()), ((Comparable) filter.getValue()));
        }
        return criteriaBuilder.greaterThan(relationalGetPath(root, filter.getFieldName()), filter.getValue().toString());
    }
}

