package org.garm.infrastructure.base.model.cache;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.Map;

@Data
@Builder
@ToString
public class Response {

    private Class tClass;
    private Map<String, Object> map;

}
