package org.garm.infrastructure.base.domain.operation;

import org.garm.infrastructure.base.dto.Filter;

import javax.persistence.criteria.Predicate;

public interface Operation {
    default Predicate execute(Filter filter) {
        return null;
    }
}
