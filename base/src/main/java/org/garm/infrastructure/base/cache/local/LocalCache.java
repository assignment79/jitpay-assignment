package org.garm.infrastructure.base.cache.local;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;

@RedisHash("localCache")
@Builder
@Data
@ToString
public class LocalCache implements Serializable {
    @Id
    private Long Id;
    @Indexed
    private String metadata;
    private String jsonObject;

}
