package org.garm.infrastructure.base.service.obj;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SmsDto {
    private String number;
    private String body;
}
