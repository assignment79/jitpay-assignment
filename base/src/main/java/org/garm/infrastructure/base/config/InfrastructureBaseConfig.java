package org.garm.infrastructure.base.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:service-path.properties")
@PropertySource("classpath:messages_en.properties")
@PropertySource("classpath:redis.properties")
@PropertySource("classpath:mongo.properties")
public class InfrastructureBaseConfig {
}
