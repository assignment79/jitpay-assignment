package org.garm.infrastructure.base.utils;

import java.util.Random;

public class RandomUtils {

    public static Integer genDigit(int digitNumber) {
        Random r = new Random(System.currentTimeMillis());
        return (int) ((1 + r.nextInt(2)) * Math.pow(10,digitNumber-1) + r.nextInt((int) Math.pow(10,digitNumber-1)));
    }
    public static Integer gen5digit() {
        Random r = new Random(System.currentTimeMillis());
        return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
    }
    public static Integer gen7digit() {
        Random r = new Random(System.currentTimeMillis());
        return ((1 + r.nextInt(2)) * 1000000 + r.nextInt(1000000));
    }
}
