package org.garm.infrastructure.base.domain;

import javax.persistence.Column;
import javax.persistence.Transient;


public interface Activation {

    @Column(name = "ACTIVE", nullable = false)
    boolean isActive();

    void setActive(boolean active);

    @Transient
    default void toggleActivation() {
        setActive(!isActive());
    }
}
