package org.garm.infrastructure.base.domain.command;

import org.garm.infrastructure.base.domain.AbstractPersistence;
import org.garm.infrastructure.base.domain.repository.BaseRepository;
import org.garm.infrastructure.base.domain.service.TransactionHandler;
import org.garm.infrastructure.base.exception.BusinessException;
import org.garm.infrastructure.base.exception.EntityNotFoundException;
import org.garm.infrastructure.base.exception.NotSameVersionException;
import org.garm.infrastructure.base.mapper.BaseMapper;
import org.garm.infrastructure.base.model.general.BaseDto;
import org.springframework.dao.EmptyResultDataAccessException;

import java.io.Serializable;
import java.util.Objects;

public interface DefaultCommandService<E extends AbstractPersistence<L>, D extends BaseDto<L>, L extends Serializable> {

    default BaseRepository<E, L> getRepository() {
        throw new UnsupportedOperationException();
    }

    default BaseMapper<E, D, L> getMapper() {
        throw new UnsupportedOperationException();
    }

    default TransactionHandler getTransactionHandler() {
        throw new UnsupportedOperationException();
    }

    default L create(D dto) {
        return create(dto, false);
    }

    default L create(D dto, boolean runInNewTransaction) {
        return runInNewTransaction
                ? getTransactionHandler().runFunctionInNewTransaction(d -> getRepository().save(getMapper().map(d)).getId(), dto)
                : getRepository().save(getMapper().map(dto)).getId();
    }

    default L createAndFlush(D dto) {
        return getRepository().saveAndFlush(getMapper().map(dto)).getId();
    }

    default void delete(L id) {
        delete(id, false);
    }

    default void delete(L id, boolean runInNewTransaction) {
        try {
            if (runInNewTransaction)
                getTransactionHandler().runConsumerInNewTransaction(i -> getRepository().deleteById(i), id);
            else
                getRepository().deleteById(id);
        } catch (EmptyResultDataAccessException exception) {
            throw new EntityNotFoundException("error.entity.not.found", id);
        }
    }

    @Deprecated
    default void update(D dto) {
        update(dto, false);
    }

    @Deprecated
    default void update(D dto, boolean runInNewTransaction) {
        beforeUpdate(dto);
        if (runInNewTransaction)
            getTransactionHandler().runConsumerInNewTransaction(d -> getRepository().save(getMapper().map(d)), dto);
        else
            getRepository().save(getMapper().map(dto));
    }

    @Deprecated
    default void updateAndFlush(D dto) {
        beforeUpdate(dto);
        getRepository().saveAndFlush(getMapper().map(dto));
    }

    default void merge(D dto) {
        merge(dto, false);
    }

    default void merge(D dto, boolean runInNewTransaction) {
        final E entity = beforeMerge(dto);
        if (runInNewTransaction)
            getTransactionHandler().runConsumerInNewTransaction(d -> getRepository().save(entity), dto);
        else
            getRepository().save(entity);
    }

    default void mergeAndFlush(D dto) {
        getRepository().saveAndFlush(beforeMerge(dto));
    }

    default void beforeUpdate(D dto) {
        if (Objects.isNull(dto.getId())) {
            throw new BusinessException("error.entity.id.should.not.null");
        }

        if (!Objects.equals(dto.getVersion(), getRepository().findById(dto.getId()).orElseThrow(
                () -> new EntityNotFoundException("error.entity.not.found", dto.getId())
        ).getVersion())) {
            throw new NotSameVersionException();
        }
    }

    default E beforeMerge(D dto) {
        if (Objects.isNull(dto.getId())) {
            throw new BusinessException("error.entity.id.should.not.null");
        }

        final E entity = getRepository().findById(dto.getId()).orElseThrow(() -> new EntityNotFoundException("error.entity.not.found", dto.getId()));

        if (!Objects.equals(dto.getVersion(), entity.getVersion())) {
            throw new NotSameVersionException();
        }

        return getMapper().merge(dto, entity);
    }
}
