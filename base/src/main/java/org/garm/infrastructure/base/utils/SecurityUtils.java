package org.garm.infrastructure.base.utils;

import org.garm.infrastructure.base.cache.AuthCacheService;
import org.garm.infrastructure.base.model.security.AuthDto;
import org.garm.infrastructure.base.model.security.CurrentUserInfoDto;
import org.garm.infrastructure.base.service.security.token.extractor.CookieAccessTokenExtractor;
import org.garm.infrastructure.base.service.security.token.extractor.TokenExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Objects;

@Service
public final class SecurityUtils {

    private static AuthCacheService authCacheService;
    public static final String TOKEN_KEY = "access-token";
    @Autowired
    private AuthCacheService authCache;

    @PostConstruct
    public void init() {
//        userInfoByTokenService = this.userInfoByToken;
        authCacheService = this.authCache;
    }

    public static String getCurrentUsername() {
        return getCurrentUser().getCurrentUsername();
    }

    public static Long getCurrentUserId() {
        return getCurrentUser().getCurrentUserId();
    }

    public static String getCurrentUserNationalId() {
        return getCurrentUser().getCurrentNationalId();
    }

    public static String getCurrentUserNationalCode() {
        return getCurrentUser().getCurrentNationalId();
    }

    public static String getCurrentUserFirstname() {
        return getCurrentUser().getCurrentFirstName();
    }

    public static String getCurrentUserLastName() {
        return getCurrentUser().getCurrentLastName();
    }

    public static String getCurrentTitle() {
        return getCurrentUser().getCurrentTitle();
    }

    public static List<Long> getCurrentUserGroups() {
        return getCurrentUser().getCurrentUserGroups();
    }

    public static CurrentUserInfoDto getCurrentUser() {
        CurrentUserInfoDto result;
        String currentUserToken = getCurrentUserToken();
        if (!StringUtils.isEmpty(currentUserToken)) {
            CurrentUserInfoDto currentUserInfoDto = getUserInfo(currentUserToken);
            result = Objects.nonNull(currentUserInfoDto) ? currentUserInfoDto : CurrentUserInfoDto.builder().build();
        } else {
            result = CurrentUserInfoDto.builder()
                    .currentUserId(-1L)
                    .currentNationalId(null)
                    .currentNationalCode(null)
                    .currentUsername("system")
                    .build();
        }
        return result;
    }

    public static CurrentUserInfoDto getUserInfo(final String token) {
        return StringUtils.isEmpty(token)
                ? CurrentUserInfoDto.builder()
                .currentUsername("no-user")
                .currentUserId(-1L)
                .build()
                : CurrentUserInfoDto.builder()
                .currentUsername(authCacheService.get(token).getUsername())
                .currentUserId(-1L)
                .build();
    }

    public static AuthDto getAuthDto() {
        return authCacheService.get(getCurrentUserToken());
    }

    private static String getCurrentUserToken() {
        TokenExtractor tokenExtractor = new TokenExtractor();
        tokenExtractor.setExtractor(new CookieAccessTokenExtractor());
        return tokenExtractor.extract() == null
                ? tokenExtractor.getExtractor().next().extract()
                : tokenExtractor.extract();
    }

    public static String extractToken(String bearerHeader) {
        return bearerHeader.replace("Bearer ", "");
    }
}
