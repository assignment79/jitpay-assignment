package org.garm.infrastructure.base.logger;

import org.slf4j.LoggerFactory;

public abstract class Logger {

    public org.slf4j.Logger logger = null;

    abstract public String getTagName();

    private void createLoggerObj() {
        if (logger == null) logger = LoggerFactory.getLogger(this.getTagName());
    }

    private void createLoggerObj(String tagName) {
        if (logger == null) logger = LoggerFactory.getLogger(tagName);
    }

    public void info(String message) {
        createLoggerObj();
        logger.info(message);
    }

    public void info(String message, String tagName) {
        createLoggerObj(tagName);
        logger.info(message);
    }

    public void warn(String message) {
        createLoggerObj();
        logger.warn(message);
    }

    public void warn(String message, String tagName) {
        createLoggerObj(tagName);
        logger.warn(message);
    }

    public void error(String message) {
        createLoggerObj();
        logger.error(message);
    }

    public void error(String message, String tagName) {
        createLoggerObj(tagName);
        logger.error(message);
    }

}
