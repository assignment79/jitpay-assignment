package org.garm.infrastructure.base.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


public final class ReflectionUtils {

    public static final int PUBLIC_FIELD = 25;

    private ReflectionUtils() {
    }

    public static <T> T convertToPrimitive(Class<T> c, Object o) {
        return (T) PrimitiveConverter.converterMapping.get(c).apply(o);
    }

    private static final class PrimitiveConverter {

        private static Map<Class, Function> converterMapping = new HashMap();

        static {
            converterMapping.put(Integer.class, (v) -> Integer.valueOf(String.valueOf(v)));
            converterMapping.put(Long.class, (v) -> Long.valueOf(String.valueOf(v)));
            converterMapping.put(String.class, (v) -> String.valueOf(v));
            converterMapping.put(Boolean.class, (v) -> Integer.valueOf(String.valueOf(v)).intValue() == 1);
            converterMapping.put(LocalDate.class,
                    (v) -> DateUtils.convertToLocalDate(String.valueOf(v), "yyyy-MM-dd"));
            converterMapping.put(LocalDateTime.class,
                    (v) -> DateUtils.convertToLocalDateTime(String.valueOf(v), "yyyy-MM-dd HH:mm:ss"));
            converterMapping.put(LocalTime.class,
                    (v) -> DateUtils.convertToLocalTime(String.valueOf(v), "HH:mm:ss"));
        }
    }
}
