package org.garm.infrastructure.base.model.service;

import com.fasterxml.jackson.annotation.JsonView;
import org.garm.infrastructure.base.enums.ResponseCode;
import org.garm.infrastructure.base.exception.ExceptionModel;
import org.garm.infrastructure.base.model.service.view.InfBaseView;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;


@Data
@ToString
@NoArgsConstructor
@Accessors(chain = true)
@JsonView({InfBaseView.ResponseView.class})
public class Response<INPUT, OUTPUT> {
    private INPUT inputArguments;
    private OUTPUT response;
    private ResponseCode responseCode = ResponseCode.GENERAL;
    private ExceptionModel errorDetail;
}