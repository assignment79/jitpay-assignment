package org.garm.infrastructure.base.domain;

import com.fasterxml.jackson.annotation.JsonView;
import org.garm.infrastructure.base.model.service.view.InfBaseView;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractTreePersistence<L extends Serializable, E extends AbstractTreePersistence<L, E>> extends AbstractPersistence<L> {

    @JsonView(InfBaseView.AbstractTreePersistence.class)
    @JoinColumn(name = "PARENT_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    protected E parent;

    @JsonView(InfBaseView.AbstractTreePersistence.class)
    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    protected List<E> children = new ArrayList<>();

    @JsonView(InfBaseView.AbstractTreePersistence.class)
    @Column(name = "PATH")
    protected String path;

    @Transient
    public Boolean isLeaf() {
        return Objects.isNull(getChildren()) || getChildren().isEmpty() ? Boolean.TRUE : Boolean.FALSE;
    }
}


