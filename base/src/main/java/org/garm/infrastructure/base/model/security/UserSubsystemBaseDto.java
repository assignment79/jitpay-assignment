package org.garm.infrastructure.base.model.security;

import org.garm.infrastructure.base.model.general.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserSubsystemBaseDto extends BaseDto<Long> {

    private SubsystemDto subsystem;

    private boolean active;

}