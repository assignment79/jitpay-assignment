package org.garm.infrastructure.base.model.general;

import com.fasterxml.jackson.annotation.JsonView;
import org.garm.infrastructure.base.model.service.view.InfBaseView;
import org.garm.infrastructure.base.validator.UpdateValidationGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class BaseDto<L extends Serializable> implements Serializable {

    @NotNull(message = "error.validation.notNull", groups = UpdateValidationGroup.class)
    @JsonView(InfBaseView.ResponseView.class)
    private L id;

    @NotNull(message = "error.validation.notNull", groups = UpdateValidationGroup.class)
    @JsonView(InfBaseView.ResponseView.class)
    private Integer version;
}
