package org.garm.infrastructure.base.logger;

import org.springframework.stereotype.Component;

@Component
public class GeneralLogger extends Logger {
    @Override
    public String getTagName() {
        return "General";
    }
}
