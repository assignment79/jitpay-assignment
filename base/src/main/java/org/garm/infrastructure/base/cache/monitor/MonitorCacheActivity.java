package org.garm.infrastructure.base.cache.monitor;

import org.garm.infrastructure.base.cache.local.LocalCacheLogger;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MonitorCacheActivity {

    private final LocalCacheLogger logger;

    @Autowired
    public MonitorCacheActivity(LocalCacheLogger logger) {
        this.logger = logger;
    }

    @AfterReturning(pointcut = "execution(* org.garm.infrastructure.base.cache.local.LocalCacheHandler.get(..))", returning = "object")
    public void findByMetadataMonitor(Object object) {
        logger.info("Redis >> " + object);
    }

    @AfterReturning(pointcut = "execution(* org.garm.infrastructure.base.cache.local.LocalCacheHandler.put(..))", returning = "object")
    public void saveMonitor(Object object) {
        logger.info("Redis [Cached] >> " + object);
    }


}