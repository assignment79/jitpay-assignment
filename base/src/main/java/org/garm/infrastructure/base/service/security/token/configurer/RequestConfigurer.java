package org.garm.infrastructure.base.service.security.token.configurer;

import feign.RequestTemplate;
import lombok.Getter;
import lombok.Setter;

import javax.servlet.http.HttpServletRequest;

@Getter
@Setter
public class RequestConfigurer implements RequestHeaderConfigurer {

    private RequestHeaderConfigurer configurer;

    @Override
    public boolean config(HttpServletRequest request, RequestTemplate requestTemplate) {
        return this.configurer.config(request, requestTemplate);
    }
}