package org.garm.assigment.mapper;

import lombok.Getter;
import org.garm.assigment.domain.JitPayUserEntity;
import org.garm.assigment.repository.JitPayUserRepository;
import org.garm.common.model.dto.JitPayUserDto;
import org.garm.infrastructure.base.mapper.BaseMapper;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.function.Supplier;

@Getter
@Mapper(componentModel = "spring")
public abstract class JitPayUserMapper implements BaseMapper<JitPayUserEntity, JitPayUserDto, Long> {

    @Autowired
    private JitPayUserRepository repository;

    @Override
    public Supplier<JitPayUserEntity> getEntity() {
        return JitPayUserEntity::new;
    }
}