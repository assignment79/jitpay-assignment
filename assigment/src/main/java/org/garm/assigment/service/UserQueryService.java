package org.garm.assigment.service;

import org.garm.assigment.domain.JitPayUserEntity;
import org.garm.common.model.dto.JitPayUserDto;
import org.garm.infrastructure.base.domain.query.DefaultQueryService;

public interface UserQueryService extends DefaultQueryService<JitPayUserEntity, JitPayUserDto, Long> {
    JitPayUserDto findByUsername(String username);
}
