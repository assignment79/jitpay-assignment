package org.garm.assigment.service.impl;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.garm.assigment.cache.JitPayUserCacheService;
import org.garm.assigment.mapper.JitPayUserMapper;
import org.garm.assigment.repository.JitPayUserRepository;
import org.garm.assigment.service.UserCommandService;
import org.garm.common.model.dto.JitPayUserDto;
import org.garm.infrastructure.base.utils.SecurityUtils;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
@Getter
public class UserCommandServiceImpl implements UserCommandService {

    private final JitPayUserRepository repository;
    private final JitPayUserMapper mapper;
    private final JitPayUserCacheService cacheService;

    @Override
    public void merge(JitPayUserDto dto) {
        String username = SecurityUtils.getAuthDto().getUsername();
        dto.setUsername(username);
        if (Objects.isNull(dto.getId())) {
            UserCommandService.super.create(dto);
        } else {
            UserCommandService.super.merge(dto);
        }
        updateCache(username, dto);
    }

    private void updateCache(String username, JitPayUserDto dto) {
        JitPayUserDto jitPayUserDto = cacheService.get(username);
        if (!Objects.isNull(jitPayUserDto)) {
            jitPayUserDto.setEmail(dto.getEmail())
                    .setFirstName(dto.getFirstName())
                    .setSecondName(dto.getSecondName());
            cacheService.put(username, jitPayUserDto);
        } else {
            cacheService.put(username, dto);
        }
    }
}
