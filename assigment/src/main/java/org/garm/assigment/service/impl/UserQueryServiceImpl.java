package org.garm.assigment.service.impl;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.garm.assigment.cache.JitPayUserCacheService;
import org.garm.assigment.mapper.JitPayUserMapper;
import org.garm.assigment.repository.JitPayUserRepository;
import org.garm.assigment.service.UserQueryService;
import org.garm.common.model.dto.JitPayUserDto;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Getter
public class UserQueryServiceImpl implements UserQueryService {

    private final JitPayUserRepository repository;
    private final JitPayUserMapper mapper;
    private final JitPayUserCacheService cacheService;

    @Override
    public JitPayUserDto findByUsername(String username) {
        return cacheService.get(username);
    }
}
