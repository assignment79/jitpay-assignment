package org.garm.assigment.service;

import org.garm.assigment.domain.JitPayUserEntity;
import org.garm.common.model.dto.JitPayUserDto;
import org.garm.infrastructure.base.domain.command.DefaultCommandService;

public interface UserCommandService extends DefaultCommandService<JitPayUserEntity, JitPayUserDto, Long> {
}
