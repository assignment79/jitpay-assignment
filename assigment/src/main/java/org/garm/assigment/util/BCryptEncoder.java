package org.garm.assigment.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCryptEncoder {

    static BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(8);


    public static void main(String[] args) {

        String password = "apsis@garm";

        System.out.println();
        System.out.println(password + " Encoded Password is : " + passwordEncoder.encode(password));
        System.out.println();

        boolean isPasswordMatch = passwordEncoder.matches(password, passwordEncoder.encode(password));
        System.out.println("Password : " + password + "   isPasswordMatch    : " + isPasswordMatch);
    }

    public static String encode(String password) {
        return passwordEncoder.encode(password);
    }
}
