package org.garm.assigment.cache;

import org.garm.common.model.dto.JitPayUserDto;
import org.garm.infrastructure.base.cache.CacheHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class JitPayUserCacheService extends CacheHandler<JitPayUserDto> {

    @Autowired
    public JitPayUserCacheService(RedisTemplate<String, Object> redisTemplate) {
        super("JitPayUser-", redisTemplate, JitPayUserDto.class);
    }

}
