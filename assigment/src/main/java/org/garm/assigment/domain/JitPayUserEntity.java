package org.garm.assigment.domain;

import lombok.Getter;
import lombok.Setter;
import org.garm.infrastructure.base.domain.AbstractAuditingEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import static org.garm.infrastructure.base.domain.AbstractPersistence.ALLOCATION_SIZE;
import static org.garm.infrastructure.base.domain.AbstractPersistence.DEFAULT_SEQ_GEN;


@Getter
@Setter
@Entity
@Table(name = JitPayUserEntity.TABLE_NAME, schema = "JT")
@SequenceGenerator(name = DEFAULT_SEQ_GEN, sequenceName = JitPayUserEntity.TABLE_NAME + "_SEQ", allocationSize = ALLOCATION_SIZE)
public class JitPayUserEntity extends AbstractAuditingEntity<Long> {

    public static final String TABLE_NAME = "JT_USER";

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "firstName", nullable = false)
    private String firstName;

    @Column(name = "secondName", nullable = false)
    private String secondName;

}

