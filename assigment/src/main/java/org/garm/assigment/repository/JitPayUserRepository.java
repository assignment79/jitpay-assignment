package org.garm.assigment.repository;

import org.garm.assigment.domain.JitPayUserEntity;
import org.garm.infrastructure.base.domain.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JitPayUserRepository extends BaseRepository<JitPayUserEntity, Long> {
}