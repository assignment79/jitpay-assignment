package org.garm.assigment.controller;

import lombok.RequiredArgsConstructor;
import org.garm.assigment.service.UserCommandService;
import org.garm.common.model.dto.JitPayUserDto;
import org.garm.infrastructure.base.model.service.Response;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserCommandController {
    private final UserCommandService service;

    @PutMapping("v1/update-info")
    public Response<JitPayUserDto, Void> updateInfo(@RequestBody JitPayUserDto dto) {
        Response<JitPayUserDto, Void> response = new Response<>();
        response.setInputArguments(dto);
        service.merge(dto);
        return response;
    }

}
