package org.garm.assigment.controller;

import lombok.RequiredArgsConstructor;
import org.garm.assigment.service.UserQueryService;
import org.garm.common.model.dto.JitPayUserDto;
import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.base.utils.SecurityUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserQueryController {
    private final UserQueryService service;

    @GetMapping("v1/latest-location")
    public Response<Void, JitPayUserDto> find() {
        Response<Void, JitPayUserDto> response = new Response<>();
        response.setResponse(service.findByUsername(SecurityUtils.getAuthDto().getUsername()));
        return response;
    }
}
