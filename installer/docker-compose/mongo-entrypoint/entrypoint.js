var db = connect("mongodb://localhost/admin");
db = db.getSiblingDB('security'); // we can not use "use" statement here to switch db
db.createUser(
    {
        user: "garm",
        pwd: "garm@35",
        roles: [ { role: "readWrite", db: "security"} ],
        passwordDigestor: "server",
    }
)
db.mongoClientDetails.insert({
        "_id": "testclient",
        "_class": "ir.rbc.infrastructuresecurity.model.MongoClientDetails",
        "clientSecret": "$2a$08$fSUhozqJWb5.RZX9aylOHu7WcQYCXsEV9oUt10mAKE2XeV.lxY3Lu",
        "scope": [
            "read",
            "write"
        ],
        "resourceIds": [
            "resource-server-rest-api"
        ],
        "authorizedGrantTypes": [
            "authorization_code",
            "password",
            "refresh_token",
            "implicit"
        ],
        "registeredRedirectUris": [
            "http://127.0.0.1:8094/login"
        ],
        "authorities": [
            {
                "role": "ROLE_CLIENT",
                "_class": "org.springframework.security.core.authority.SimpleGrantedAuthority"
            }
        ],
        "accessTokenValiditySeconds": 30000,
        "refreshTokenValiditySeconds": 30000,
        "additionalInformation": {},
        "autoApproveScopes": []
    }
)    

db.user.insert({
        "_id" : "garm",
        "_class": "org.garm.infrastructure.security.domain.document.User",
        "password": "$2a$08$fSUhozqJWb5.RZX9aylOHu7WcQYCXsEV9oUt10mAKE2XeV.lxY3Lu",
        "authorities": [],
        "accountNonExpired": false,
        "accountNonLocked": false,
        "credentialsNonExpired": false,
        "enabled": true
    }    
)
