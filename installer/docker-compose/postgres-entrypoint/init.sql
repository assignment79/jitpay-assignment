create database jtdb;
create user jt with encrypted password 'jt$$123';
grant all privileges on database jtdb to jt;
\c jtdb;
create schema jt;
alter schema jt owner to jt;
