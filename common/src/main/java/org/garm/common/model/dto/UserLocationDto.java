package org.garm.common.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;
import org.garm.infrastructure.base.model.general.AuditingDto;
import org.garm.infrastructure.base.model.general.BaseDto;

@Data
@Accessors(chain = true)
public class UserLocationDto extends AuditingDto<Long> {
    private String username;
    private double latitude;
    private double longitude;
}
