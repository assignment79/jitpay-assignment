package org.garm.common.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;
import org.garm.common.model.dto.UserLocationDto;
import org.garm.infrastructure.base.model.general.AuditingDto;

@Data
@Accessors(chain = true)
public class JitPayUserDto extends AuditingDto<Long> {
    @JsonIgnore
    private String username;
    private String email;
    private String firstName;
    private String secondName;
    private UserLocationDto location;
}
