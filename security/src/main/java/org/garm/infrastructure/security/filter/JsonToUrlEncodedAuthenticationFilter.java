package org.garm.infrastructure.security.filter;

import org.garm.infrastructure.security.service.feign.FindUserByUserNameService;
import org.garm.infrastructure.base.service.call.SmsSenderService;
import org.garm.infrastructure.security.cache.CaptchaCacheService;
import org.garm.infrastructure.security.cache.OtpCacheService;
import org.garm.infrastructure.security.login.OtpLoginStep1;
import org.garm.infrastructure.security.login.OtpLoginStep2;
import org.garm.infrastructure.security.login.RestLogin;
import org.garm.infrastructure.security.login.WebLogin;
import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.garm.infrastructure.security.util.SecurityConstant.*;

@Component
@RequiredArgsConstructor
@Order(value = Integer.MIN_VALUE)
public class JsonToUrlEncodedAuthenticationFilter extends GenericFilterBean {

    private final CaptchaCacheService captchaCache;
    private final SmsSenderService smsSenderService;
    private final FindUserByUserNameService userService;
    private final OtpCacheService otpCacheService;


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        String requestURI = ((HttpServletRequest) request).getRequestURI();

        switch (((HttpServletRequest) request).getRequestURI()) {
            case LOGIN_WEB_URL:
                new WebLogin(requestURI, captchaCache).login(request, response, chain);
                break;
            case LOGIN_REST_URL:
                new RestLogin(requestURI).login(request, response, chain);
                break;
            case LOGIN_OTP_STEP1:
                new OtpLoginStep1(requestURI, smsSenderService, userService, otpCacheService, captchaCache).login(request, response, chain);
                break;
            case LOGIN_OTP_STEP2:
                new OtpLoginStep2(requestURI, otpCacheService).login(request, response, chain);
                break;
            default:
                chain.doFilter(request, response);
        }

    }
}