package org.garm.infrastructure.security.service.feign.impl;

import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.security.domain.dto.ChangePasswordDto;
import org.garm.infrastructure.security.domain.dto.ForgetPasswordDto;
import org.garm.infrastructure.security.service.BeanCreationCondition;
import org.garm.infrastructure.security.service.feign.UserPasswordService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Conditional(BeanCreationCondition.class)
@FeignClient(url = "${user.management.base.url}", contextId = "user-management-user-password", value = "USER-MANAGEMENT")
public interface UserPasswordFeign extends UserPasswordService {

    @PostMapping("/facade/users/change-user-password")
    @Override
    Response<ChangePasswordDto, Void> changeUserPassword(@RequestBody ChangePasswordDto passwordDto);

    @PostMapping("/facade/sms/sms-send")
    @Override
    Response<ForgetPasswordDto, Void> sendSms(@RequestBody ForgetPasswordDto dto);

    @PostMapping("/facade/sms/reset-password")
    @Override
    Response<ForgetPasswordDto, Void> resetPassword(@RequestBody ForgetPasswordDto dto);

    @PostMapping("/facade/sms/forget-password")
    @Override
    Response<ForgetPasswordDto, Void> forgetPassword(@RequestBody ForgetPasswordDto dto);

}

