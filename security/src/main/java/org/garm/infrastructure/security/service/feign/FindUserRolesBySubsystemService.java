package org.garm.infrastructure.security.service.feign;

import org.garm.infrastructure.base.model.service.Response;

import java.util.List;

public interface FindUserRolesBySubsystemService {
    Response<String, List<String>> getCurrentUserRoles(String username);
}
