package org.garm.infrastructure.security.service.servicepath;

public interface ServicePathUrlService {
    void hasAccessUrlThisPath(String userName, String url);
}
