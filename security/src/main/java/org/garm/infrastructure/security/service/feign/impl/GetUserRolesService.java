package org.garm.infrastructure.security.service.feign.impl;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.base.utils.DefaultFallBackBuilder;
import org.garm.infrastructure.security.service.BeanCreationCondition;
import org.garm.infrastructure.security.service.feign.FindUserRolesBySubsystemService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Conditional(BeanCreationCondition.class)
@FeignClient(url = "${user.management.base.url}", contextId = "SECURITY-GET-CURRENT-USER-ROLES", value = "USER-MANAGEMENT")
public interface GetUserRolesService extends FindUserRolesBySubsystemService {

    @CircuitBreaker(name = "GET-SECURITY-GET-CURRENT-USER-ROLES", fallbackMethod = "doFallBack")
    @GetMapping(value = "/user/get-current-user-roles/{subsystem}")
    Response<String, List<String>> apply(@PathVariable String subsystem);

    default Response<String, List<String>> doFallBack(String subsystem, Throwable throwable) {
        return DefaultFallBackBuilder.build(subsystem, throwable);
    }
}

