package org.garm.infrastructure.security.filter;

import org.garm.infrastructure.base.cache.AuthCacheService;
import org.garm.infrastructure.base.model.security.AuthDto;
import org.garm.infrastructure.base.model.security.UserDetailsDto;
import org.garm.infrastructure.security.cache.CaptchaCacheService;
import org.garm.infrastructure.security.cache.TokenCacheService;
import org.garm.infrastructure.security.cache.UserCacheService;
import org.garm.infrastructure.security.domain.dto.CaptchaDto;
import org.garm.infrastructure.security.helper.TokenHelper;
import org.garm.infrastructure.security.provider.MongoTokenStore;
import org.garm.infrastructure.security.service.message.SecurityMessagingService;
import org.garm.infrastructure.security.wrapper.CustomServletResponseWrapper;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import static org.garm.infrastructure.security.util.SecurityConstant.*;


public class CustomAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final TokenCacheService tokenCache;
    private final UserCacheService userCache;
    private final AuthCacheService authCache;
    private final MongoTokenStore tokenStore;
    private final ThreadLocal<UserDetailsDto> users;
    private final CaptchaCacheService captchaCache;
    private final SecurityMessagingService securityMessagingService;
    private final Environment env;

    public CustomAuthenticationFilter(TokenCacheService tokenCache,
                                      AuthCacheService authCache, MongoTokenStore tokenStore,
                                      UserCacheService userProfileCache,
                                      CaptchaCacheService captchaCache,
                                      SecurityMessagingService securityMessagingService,
                                      Environment env) {
        super(LOGIN_URL);
        this.tokenCache = tokenCache;
        this.authCache = authCache;
        this.tokenStore = tokenStore;
        this.userCache = userProfileCache;
        this.securityMessagingService = securityMessagingService;
        this.users = new ThreadLocal<>();
        this.captchaCache = captchaCache;
        this.env = env;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        String username = request.getParameter(USERNAME);
        String password = request.getParameter(PASSWORD);

        TokenHelper.removeTokens(Collections.singletonList(username), request.getUserPrincipal().getName());
        users.set(new UserDetailsDto().setUserName(username).setPassword(password));
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        return getAuthenticationManager().authenticate(token);
    }

    @Override
    protected void successfulAuthentication(
            final HttpServletRequest request, final HttpServletResponse response,
            final FilterChain chain, final Authentication authResult)
            throws IOException, ServletException {

        getSuccessHandler().onAuthenticationSuccess(request, new CustomServletResponseWrapper(response, env), chain, authResult);

        captchaCache.put(request.getHeader(CAPTCHA_KEY), new CaptchaDto());
        Collection<OAuth2AccessToken> tokensByUserName = tokenStore.findTokensByClientIdAndUserName(request.getUserPrincipal().getName(), authResult.getName());
        if (CollectionUtils.isNotEmpty(tokensByUserName)) {
            String token = tokensByUserName.iterator().next().getValue();
            tokenCache.put(authResult.getName(), token);
            userCache.put(authResult.getName(), users.get());
            authCache.put(token, new AuthDto().setClientId(request.getUserPrincipal().getName()).setUsername(authResult.getName()));
        }
//        securityMessagingService.sendLoginSms(authResult.getName(), request.getUserPrincipal().getName());
    }
}