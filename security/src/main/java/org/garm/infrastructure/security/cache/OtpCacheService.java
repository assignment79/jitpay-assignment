package org.garm.infrastructure.security.cache;

import org.garm.infrastructure.base.cache.CacheHandler;
import org.garm.infrastructure.security.domain.dto.OtpDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class OtpCacheService extends CacheHandler<OtpDto> {

    @Autowired
    public OtpCacheService(RedisTemplate<String, Object> redisTemplate) {
        super("otp-", redisTemplate, OtpDto.class);
    }

}
