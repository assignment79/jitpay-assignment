package org.garm.infrastructure.security.repository.mongo;


import org.garm.infrastructure.security.domain.document.MongoOAuth2RefreshToken;

public interface MongoOAuth2RefreshTokenRepositoryBase {
    MongoOAuth2RefreshToken findByTokenId(String tokenId);

    boolean deleteByTokenId(String tokenId);
}
