package org.garm.infrastructure.security.cache;

import org.garm.infrastructure.base.cache.CacheHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserVerificationCodeCacheService extends CacheHandler<String> {

    @Autowired
    public UserVerificationCodeCacheService(RedisTemplate<String, Object> redisTemplate) {
        super("user-verification-code-", redisTemplate, String.class);
    }

}
