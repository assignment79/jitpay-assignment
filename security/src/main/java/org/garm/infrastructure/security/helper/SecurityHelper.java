package org.garm.infrastructure.security.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.garm.infrastructure.base.exception.BaseException;
import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.base.service.obj.VoidObject;
import org.garm.infrastructure.security.cache.UserCacheService;
import org.garm.infrastructure.security.cache.UserInfoCacheService;
import org.garm.infrastructure.security.domain.dto.TokenDto;
import org.garm.infrastructure.base.model.security.UserDetailsDto;
import org.garm.infrastructure.security.domain.dto.UserInfoDto;
import org.garm.infrastructure.security.util.SecurityConstant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

import static org.garm.infrastructure.security.util.SecurityConstant.TOKEN_KEY;


@Component
public class SecurityHelper {

    private static UserInfoCacheService userInfoCache;
    private static UserCacheService userProfileCache;

    @Autowired
    private UserInfoCacheService userInfoCacheService;
    @Autowired
    private UserCacheService userProfileCacheService;

    @PostConstruct
    public void init() {
        userInfoCache = this.userInfoCacheService;
        userProfileCache = this.userProfileCacheService;
    }

    public static Cookie setCookie(final String Key, final String content) {
        return new Cookie(Key, content);
    }

    public static void removeCookie(HttpServletResponse response, String key) {
        Cookie cookie = setCookie(key, "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }

    public static void clearCookie(HttpServletResponse response) {
        SecurityHelper.removeCookie(response, TOKEN_KEY);
    }

    public static void clearUserInfoCache(String username) {
        userInfoCache.put(username, new UserInfoDto());
        userProfileCache.put(username, new UserDetailsDto());
    }

    public static void setCharset(HttpServletResponse response) {
        response.setHeader("Content-type", "text/html; charset=utf-8");
    }

    public static boolean hasSuperAccess(String url) {
        return Arrays.asList(SecurityConstant.SpecialPath.LOGOUT).contains(url);
    }

    public static byte[] setResponse(final String content) throws JsonProcessingException {
        if (StringUtils.isEmpty(content)) throw new BaseException();
        ObjectMapper objectMapper = new ObjectMapper();
        Response<VoidObject, TokenDto> response = new Response<>();
        response.setResponse(objectMapper.readValue(content, TokenDto.class));
        return objectMapper.writeValueAsString(response).getBytes();
    }

    public static boolean IsPasswordMatch(String password, String encodedPassword) {
        return new BCryptPasswordEncoder(8).matches(password, encodedPassword);
    }

    public static String EncryptPassword(String password) {
        return new BCryptPasswordEncoder(8).encode(password);
    }

    public static String convertObjectToJson(Object object) throws JsonProcessingException {
        if (object == null) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }

    public static String getClientId() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return ((OAuth2Authentication) auth).getOAuth2Request().getClientId();
    }
}
