package org.garm.infrastructure.security.service.feign;

import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.security.domain.dto.AuthenticationDto;
import org.garm.infrastructure.security.domain.dto.UserInfoDto;

public interface GetUserinfoService {
    Response<AuthenticationDto, UserInfoDto> getUserInfo(AuthenticationDto authentication);
}
