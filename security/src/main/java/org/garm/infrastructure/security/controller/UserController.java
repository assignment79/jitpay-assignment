package org.garm.infrastructure.security.controller;

import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.security.cache.UserInfoCacheService;
import org.garm.infrastructure.security.domain.dto.AuthenticationDto;
import org.garm.infrastructure.security.domain.dto.ChangePasswordDto;
import org.garm.infrastructure.security.domain.dto.UserInfoDto;
import org.garm.infrastructure.security.helper.SecurityHelper;
import org.garm.infrastructure.security.provider.MongoTokenStore;
import org.garm.infrastructure.security.service.feign.UserPasswordService;
import org.garm.infrastructure.security.service.feign.GetUserinfoService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user/")
public class UserController {

    private final MongoTokenStore tokenStore;
    private final UserInfoCacheService userInfoCacheService;
    private final GetUserinfoService userInfoService;
    private final UserPasswordService userPasswordService;

    @PostMapping("logout")
    public void logOut(HttpServletResponse response, Authentication authentication) {
        Collection<OAuth2AccessToken> tokensByUserName = tokenStore.findTokensByClientIdAndUserName(SecurityHelper.getClientId(), authentication.getName());
        if (CollectionUtils.isNotEmpty(tokensByUserName)) {
            tokenStore.removeAccessToken(tokenStore.readAccessToken(tokensByUserName.iterator().next().getValue()));
            tokenStore.removeRefreshToken(tokensByUserName.iterator().next().getRefreshToken());
            userInfoCacheService.put(authentication.getName(), new UserInfoDto());
        }
    }

    @PostMapping("get-user-info")
    public Response<AuthenticationDto, UserInfoDto> getUserInfo(Authentication auth) {
        AuthenticationDto dto = new AuthenticationDto();
        dto.setUsername(auth.getName());
        dto.setClient_id(((OAuth2Authentication) auth).getOAuth2Request().getClientId());
        return userInfoService.getUserInfo(dto);
    }

    @PostMapping("change-user-password")
    public Response<ChangePasswordDto, Void> changeUserPassword(Authentication auth, @RequestBody ChangePasswordDto passwordDto) {
        passwordDto.setUsername(auth.getName());
        passwordDto.setClientId(((OAuth2Authentication) auth).getOAuth2Request().getClientId());
        return userPasswordService.changeUserPassword(passwordDto);
    }

}
