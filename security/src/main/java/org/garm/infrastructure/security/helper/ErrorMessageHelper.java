package org.garm.infrastructure.security.helper;

import org.garm.infrastructure.base.config.PropertiesHandler;
import org.garm.infrastructure.base.enums.ResponseCode;
import org.garm.infrastructure.base.exception.BaseException;
import org.garm.infrastructure.base.exception.ExceptionModel;
import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.base.service.obj.VoidObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.garm.infrastructure.security.helper.SecurityHelper.setCharset;

@Component
public class ErrorMessageHelper {


    public static void setErrorMessage(HttpServletResponse response, String message) throws IOException {
        Response<VoidObject, BaseException> errorResponse = new Response<>();
        response.setStatus(HttpStatus.OK.value());
        setCharset(response);
        errorResponse.setResponseCode(ResponseCode.EXCEPTION);
        errorResponse.setErrorDetail(new ExceptionModel().setMessage(message == null ? "" : new PropertiesHandler().getValue(message)));
        response.getWriter().write(SecurityHelper.convertObjectToJson(errorResponse));
    }

    public static void setException(HttpServletResponse response, String messageCode) {
        try {
            response.setStatus(HttpStatus.OK.value());
            setCharset(response);
            response.getWriter().write(new PropertiesHandler().getValue(messageCode));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void setErrorMessage(HttpServletResponse response, String message, ResponseCode responseCode) throws IOException {
        Response<VoidObject, BaseException> errorResponse = new Response<>();
        response.setStatus(HttpStatus.OK.value());
        setCharset(response);
        errorResponse.setResponseCode(responseCode);
        errorResponse.setErrorDetail(new ExceptionModel().setMessage(message == null ? "" : new PropertiesHandler().getValue(message)));
        response.getWriter().write(SecurityHelper.convertObjectToJson(errorResponse));
    }

}
