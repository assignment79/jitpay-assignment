package org.garm.infrastructure.security.service.feign;

import org.garm.infrastructure.base.model.security.UserDetailsDto;
import org.garm.infrastructure.base.model.service.Response;

public interface FindUserByUserNameService {
    Response<String, UserDetailsDto> findUserByUsername(String username);
}
