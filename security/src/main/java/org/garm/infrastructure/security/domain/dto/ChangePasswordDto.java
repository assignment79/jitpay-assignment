package org.garm.infrastructure.security.domain.dto;

import org.garm.infrastructure.base.model.security.AuthDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ChangePasswordDto extends AuthDto {
    private String currentPassword;
    private String newPassword;
}
