package org.garm.infrastructure.security.service;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySource;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Objects;

public class BeanCreationCondition implements Condition {

    private static final String PREFIX = "spring.application.name";
    private static final String APP_NAME = "USER-MANAGEMENT";

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        Environment env = context.getEnvironment();
        if (env instanceof ConfigurableEnvironment) {
            for (PropertySource<?> propertySource : ((ConfigurableEnvironment) env).getPropertySources()) {
                if (propertySource instanceof EnumerablePropertySource) {
                    for (String key : ((EnumerablePropertySource<?>) propertySource).getPropertyNames()) {
                        if (key.startsWith(PREFIX) && !Objects.equals(propertySource.getProperty(key), APP_NAME)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}