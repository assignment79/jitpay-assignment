package org.garm.infrastructure.security.service.userdetail;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.garm.infrastructure.security.repository.mongo.UserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.security.auth.login.CredentialException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository service;

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<org.garm.infrastructure.security.domain.document.User> user = service.findByUsername(username);

        if (user.isEmpty()) {
            throw new UsernameNotFoundException("security.bad.credencial");
        } else if (!user.get().isEnabled()) {
            throw new CredentialException("security.bad.user.disabled");
        }

        return new User(user.get().getUsername(), user.get().getPassword(), getAuthority());
    }

    private List<SimpleGrantedAuthority> getAuthority() {
        return Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"));
    }

}