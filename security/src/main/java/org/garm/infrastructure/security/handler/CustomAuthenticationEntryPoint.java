package org.garm.infrastructure.security.handler;

import org.garm.infrastructure.base.enums.ResponseCode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.garm.infrastructure.security.helper.ErrorMessageHelper.setErrorMessage;


@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Value("${security.invalid.access.token}")
    private String message;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authenticationException) throws IOException, ServletException {

        if (authenticationException instanceof InsufficientAuthenticationException)
            setErrorMessage(response, message, ResponseCode.UNAUTHORIZED);
        else
            setErrorMessage(response, message);
    }

}