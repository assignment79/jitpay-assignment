package org.garm.infrastructure.security.service.feign;

import org.garm.infrastructure.base.model.security.SubsystemDto;
import org.garm.infrastructure.base.model.service.Response;

public interface FindSubsystemByNameService {
    Response<String, SubsystemDto> find(String username);
}
