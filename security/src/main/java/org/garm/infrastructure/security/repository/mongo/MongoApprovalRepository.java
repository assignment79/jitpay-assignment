package org.garm.infrastructure.security.repository.mongo;

import org.garm.infrastructure.security.domain.document.MongoApproval;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MongoApprovalRepository extends MongoRepository<MongoApproval, String>, MongoApprovalRepositoryBase {
}
