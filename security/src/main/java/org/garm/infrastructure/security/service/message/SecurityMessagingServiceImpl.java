package org.garm.infrastructure.security.service.message;

import org.garm.infrastructure.base.config.PropertiesHandler;
import org.garm.infrastructure.base.enums.ResponseCode;
import org.garm.infrastructure.base.exception.BusinessException;
import org.garm.infrastructure.base.model.security.SubsystemDto;
import org.garm.infrastructure.base.model.security.UserDetailsDto;
import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.base.service.call.SmsSenderService;
import org.garm.infrastructure.base.service.obj.SmsDto;
import org.garm.infrastructure.base.utils.DateUtils;
import org.garm.infrastructure.base.utils.RandomUtils;
import org.garm.infrastructure.security.cache.ForgetPasswordCacheService;
import org.garm.infrastructure.security.service.feign.FindSubsystemByNameService;
import org.garm.infrastructure.security.service.feign.FindUserByUserNameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.garm.infrastructure.security.util.SecurityConstant.USER_MANAGEMENT;

@Slf4j
@Service
@RequiredArgsConstructor
public class SecurityMessagingServiceImpl implements SecurityMessagingService {

    private final FindUserByUserNameService userService;
    private final SmsSenderService smsSenderService;
    private final FindSubsystemByNameService subsystemService;
    @Value("${security.sms.send.login.success.enabled}")
    private boolean loginSuccessEnabled;
    private final ForgetPasswordCacheService cache;


    public void sendLoginSms(String username, String subsystem) {
        if (!loginSuccessEnabled) return;
        Response<String, UserDetailsDto> user = userService.findUserByUsername(username);
        LocalDateTime now = LocalDateTime.now();
        smsSenderService.apply(USER_MANAGEMENT, Collections.singletonList(SmsDto.builder()
                .number(user.getResponse().getMobileNumber())
                .body(new PropertiesHandler().getValue("security.sms.send.login.success",
                        user.getResponse().getFirstName() + " " + user.getResponse().getLastName(),
                        DateUtils.convertToJalaliDateString(DateUtils.convertToJalaliDate(now.toLocalDate()), DateUtils.DATE_FORMAT_2),
                        DateUtils.convertToJalaliString(now.toLocalTime()),
                        getSubsystem(subsystem))).build()));
    }

    private String getSubsystem(String subsystemName) {
        Response<String, SubsystemDto> subsystem = subsystemService.find(subsystemName);
        if (Objects.nonNull(subsystemName)) {
            subsystem = subsystemService.find(subsystemName.toLowerCase());
        }
        return subsystem.getResponse().getTitle();
    }

    @Override
    public void sendSms(String username) {
        Response<String, UserDetailsDto> user = userService.findUserByUsername(username);
        if (Objects.isNull(user) || Objects.isNull(user.getResponse())) {
            throw new BusinessException("security.username.not.found");
        }
        String mobileNumber = user.getResponse().getMobileNumber();

        log.error("SEND SMS METHOD   &&&&&&&&&&&&&&&&&&&&&&&&&");
        log.error(mobileNumber);

        Integer referralCode = RandomUtils.gen5digit();
        log.error("call service with this number");
        log.error(Collections.singletonList(SmsDto.builder().number(mobileNumber).build()).get(0).getNumber());

        Response<List<SmsDto>, Void> response = smsSenderService.apply(USER_MANAGEMENT,
                Collections.singletonList(SmsDto.builder()
                        .number(mobileNumber)
                        .body(new PropertiesHandler().getValue("security.sms.reset.password.content", referralCode))
                        .build()));

        if (response.getResponseCode() != ResponseCode.GENERAL) {
            log.error("sms service");
            log.error(smsSenderService.toString());
            log.error("sms url");
            log.error("error code is");
            log.error("getResponseCode code is");
            log.error(response.getResponseCode().getValue().toString());
            log.error("getErrorCode code is");
            throw new BusinessException("security.sms.failed");
        }
        cache.put(mobileNumber, referralCode);
    }
}
