package org.garm.infrastructure.security.cache;

import org.garm.infrastructure.base.cache.CacheHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserUuidCacheService extends CacheHandler<String> {

    @Autowired
    public UserUuidCacheService(RedisTemplate<String, Object> redisTemplate) {
        super("user-uuid-", redisTemplate, String.class);
    }

}
