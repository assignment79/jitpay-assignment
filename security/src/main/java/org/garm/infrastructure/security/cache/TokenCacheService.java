package org.garm.infrastructure.security.cache;

import org.garm.infrastructure.base.cache.CacheHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class TokenCacheService extends CacheHandler<String> {

    @Autowired
    public TokenCacheService(RedisTemplate<String, Object> redisTemplate) {
        super("token-", redisTemplate, String.class);
    }

}
