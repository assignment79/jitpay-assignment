package org.garm.infrastructure.security.login;

import org.garm.infrastructure.base.config.PropertiesHandler;
import org.garm.infrastructure.base.service.obj.SmsDto;
import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.base.model.security.UserDetailsDto;
import org.garm.infrastructure.security.service.feign.FindUserByUserNameService;
import org.garm.infrastructure.base.service.call.SmsSenderService;
import org.garm.infrastructure.base.utils.RandomUtils;
import org.garm.infrastructure.security.cache.CaptchaCacheService;
import org.garm.infrastructure.security.cache.OtpCacheService;
import org.garm.infrastructure.security.domain.dto.AuthenticationDto;
import org.garm.infrastructure.security.domain.dto.OtpDto;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

import static org.garm.infrastructure.security.util.SecurityConstant.CAPTCHA_KEY;
import static org.garm.infrastructure.security.util.SecurityConstant.USER_MANAGEMENT;

public class OtpLoginStep1 extends AbstractLogin {

    private final SmsSenderService smsSenderService;
    private final FindUserByUserNameService userService;
    private final OtpCacheService otpCacheService;
    private final CaptchaCacheService captchaCache;

    public OtpLoginStep1(String loginUrl, SmsSenderService smsSenderService,
                         FindUserByUserNameService userService, OtpCacheService otpCacheService, CaptchaCacheService captchaCache) {
        super(loginUrl);
        this.smsSenderService = smsSenderService;
        this.userService = userService;
        this.otpCacheService = otpCacheService;
        this.captchaCache = captchaCache;
    }

    @SneakyThrows
    @Override
    public void validate(ServletRequest request, ServletResponse response, AuthenticationDto auth, HashMap<String, String[]> params) {

        String captcha_key = ((HttpServletRequest) request).getHeader(CAPTCHA_KEY);

        if (captcha_key == null
                || StringUtils.isEmpty(auth.getCaptcha())
                || Objects.isNull(captchaCache.get(captcha_key))
                || Objects.isNull(captchaCache.get(captcha_key).getAnswer())
                || !captchaCache.get(captcha_key).getAnswer().equals(auth.getCaptcha())) {

            expireCaptcha(captchaCache, captcha_key);
            setError((HttpServletResponse) response, "security.captcha.failed", params);

        } else {
            String body = getObjectMapper().writeValueAsString(new OtpDto().setSmsIdentifier(sendMessage(auth.getUsername())));
            setResponse((HttpServletResponse) response, body, params);
        }

    }

    private String sendMessage(String username) {
        String smsIdentifier = null;
        Response<String, UserDetailsDto> user = userService.findUserByUsername(username);
        if (!Objects.isNull(user)) {
            smsIdentifier = UUID.randomUUID().toString();
            String verificationCode = RandomUtils.genDigit(5).toString();

            smsSenderService.apply(USER_MANAGEMENT, Collections.singletonList(SmsDto.builder()
                    .number(user.getResponse().getMobileNumber())
                    .body(new PropertiesHandler().getValue("security.otp.message") + " " + verificationCode)
                    .build()));
            otpCacheService.put(smsIdentifier, new OtpDto().setSmsIdentifier(smsIdentifier).setVerificationCode(verificationCode), 3);
        }
        return smsIdentifier;
    }


}
