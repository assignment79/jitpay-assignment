package org.garm.infrastructure.security.controller;

import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.security.domain.dto.ForgetPasswordDto;
import org.garm.infrastructure.security.service.feign.UserPasswordService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/resource/")
public class SmsController {

    private final UserPasswordService service;

    @PostMapping("sms-send")
    public Response<ForgetPasswordDto, Void> sendSms(@RequestBody ForgetPasswordDto dto) {
        return service.sendSms(dto);
    }

    @PostMapping("reset-password")
    public Response<ForgetPasswordDto, Void> resetPassword(@RequestBody ForgetPasswordDto dto) {
        return service.resetPassword(dto);
    }

    @PostMapping("forget-password")
    public Response<ForgetPasswordDto, Void> forgetPassword(@RequestBody ForgetPasswordDto dto) {
        return service.forgetPassword(dto);
    }

}
