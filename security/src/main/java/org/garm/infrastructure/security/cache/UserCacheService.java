package org.garm.infrastructure.security.cache;

import org.garm.infrastructure.base.cache.CacheHandler;
import org.garm.infrastructure.base.model.security.UserDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserCacheService extends CacheHandler<UserDetailsDto> {

    @Autowired
    public UserCacheService(RedisTemplate<String, Object> redisTemplate) {
        super("user-profile-", redisTemplate, UserDetailsDto.class);
    }

}
