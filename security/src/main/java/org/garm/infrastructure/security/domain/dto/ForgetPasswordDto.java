package org.garm.infrastructure.security.domain.dto;

import org.garm.infrastructure.security.domain.dto.CaptchaDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ForgetPasswordDto extends CaptchaDto {
    private Integer referralCode;
    private String newPassword;
    private String userName;
}
