package org.garm.infrastructure.security;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@EnableFeignClients
@SpringBootApplication
@ComponentScan("org.garm")
public class InfrastructureSecurity {

    public static void main(String[] args) {
        SpringApplication.run(InfrastructureSecurity.class, args);
    }

}
