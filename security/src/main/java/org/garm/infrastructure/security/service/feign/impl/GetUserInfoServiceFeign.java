package org.garm.infrastructure.security.service.feign.impl;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.base.utils.DefaultFallBackBuilder;
import org.garm.infrastructure.security.domain.dto.AuthenticationDto;
import org.garm.infrastructure.security.domain.dto.UserInfoDto;
import org.garm.infrastructure.security.service.BeanCreationCondition;
import org.garm.infrastructure.security.service.feign.GetUserinfoService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Conditional(BeanCreationCondition.class)
@FeignClient(url = "${user.management.base.url}", contextId = "user-management-get-user-Info", value = "USER-MANAGEMENT")
public interface GetUserInfoServiceFeign extends GetUserinfoService {

    @CircuitBreaker(name = "get-user-info", fallbackMethod = "doFallBack")
    @PostMapping("/facade/users/get-user-information")
    @Override
    Response<AuthenticationDto, UserInfoDto> getUserInfo(@RequestBody AuthenticationDto authentication);

    default Response<AuthenticationDto, UserInfoDto> doFallBack(AuthenticationDto authentication, Throwable throwable) {
        return DefaultFallBackBuilder.build(authentication, throwable);
    }

}

