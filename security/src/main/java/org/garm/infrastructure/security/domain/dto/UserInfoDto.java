package org.garm.infrastructure.security.domain.dto;

import org.garm.infrastructure.base.model.security.UserDetailsDto;
import org.garm.infrastructure.base.model.security.UserSubsystemBaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserInfoDto extends UserDetailsDto {

    private Long id;
    private String email;
    private Long activeSubsystemId;
    private String activeSubsystemName;
    private List<String> actions;
    private List<String> permissions;
    private Set<UserSubsystemBaseDto> userSubsystems;
    private boolean forcePasswordChange;
    private boolean vip;
    private boolean empty;

    public boolean isEmpty() {
        return StringUtils.isEmpty(getFirstName()) || StringUtils.isEmpty(getLastName());
    }
}
