package org.garm.infrastructure.security.cache;

import org.garm.infrastructure.base.cache.CacheHandler;
import org.garm.infrastructure.security.domain.dto.UserInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserInfoCacheService extends CacheHandler<UserInfoDto> {

    @Autowired
    public UserInfoCacheService(RedisTemplate<String, Object> redisTemplate) {
        super("user-info-", redisTemplate, UserInfoDto.class);
    }

}
