package org.garm.infrastructure.security.cache;

import org.garm.infrastructure.base.cache.CacheHandler;
import org.garm.infrastructure.security.domain.dto.CaptchaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class CaptchaCacheService extends CacheHandler<CaptchaDto> {

    @Autowired
    public CaptchaCacheService(RedisTemplate<String, Object> redisTemplate) {
        super("captcha-", redisTemplate, CaptchaDto.class);
    }

}
