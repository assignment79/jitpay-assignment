package org.garm.infrastructure.security.service.feign.impl;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.garm.infrastructure.base.model.security.UserDetailsDto;
import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.base.utils.DefaultFallBackBuilder;
import org.garm.infrastructure.security.service.BeanCreationCondition;
import org.garm.infrastructure.security.service.feign.FindUserByUserNameService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Conditional(BeanCreationCondition.class)
@FeignClient(url = "${user.management.base.url}", contextId = "user-management-service-find-user", value = "USER-MANAGEMENT")
public interface GetUserServiceFeign extends FindUserByUserNameService {

    @CircuitBreaker(name = "find-user-by-username", fallbackMethod = "doFallBack")
    @PostMapping("/facade/users/get-user-by-username")
    Response<String, UserDetailsDto> findUserByUsername(@RequestBody String username);

    default Response<String, UserDetailsDto> doFallBack(String username, Throwable throwable) {
        return DefaultFallBackBuilder.build(username, throwable);
    }

}

