package org.garm.infrastructure.security.service.feign;

import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.security.domain.dto.ChangePasswordDto;
import org.garm.infrastructure.security.domain.dto.ForgetPasswordDto;

public interface UserPasswordService {

    Response<ChangePasswordDto, Void> changeUserPassword(ChangePasswordDto passwordDto);

    Response<ForgetPasswordDto, Void> sendSms(ForgetPasswordDto dto);

    Response<ForgetPasswordDto, Void> resetPassword(ForgetPasswordDto dto);

    Response<ForgetPasswordDto, Void> forgetPassword(ForgetPasswordDto dto);

}
