package org.garm.infrastructure.security.service.feign.impl;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.garm.infrastructure.base.model.security.SubsystemDto;
import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.base.utils.DefaultFallBackBuilder;
import org.garm.infrastructure.security.service.BeanCreationCondition;
import org.garm.infrastructure.security.service.feign.FindSubsystemByNameService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Conditional(BeanCreationCondition.class)
@FeignClient(url = "${user.management.base.url}", contextId = "user-management-service-subsystem", value = "USER-MANAGEMENT")
public interface GetSubsystemService extends FindSubsystemByNameService {

    @CircuitBreaker(name = "subsystem-find-by-name", fallbackMethod = "doFallBack")
    @PostMapping("/subsystem/find-by-name")
    Response<String, SubsystemDto> find(@RequestBody String subsystem);

    default Response<String, SubsystemDto> doFallBack(String username, Throwable throwable) {
        return DefaultFallBackBuilder.build(username, throwable);
    }
}
