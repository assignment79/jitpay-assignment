package org.garm.infrastructure.security.repository.log;

import org.garm.infrastructure.security.domain.document.RequestLog;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestLogRepository extends MongoRepository<RequestLog, Long> {
}