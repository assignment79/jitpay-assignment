package org.garm.infrastructure.security.service.message;

public interface SecurityMessagingService {

    void sendLoginSms(String username, String subsystem);

    void sendSms(String username);

}
