package org.garm.infrastructure.security.repository.mongo;

import org.garm.infrastructure.security.domain.document.MongoClientDetails;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MongoClientDetailsRepository extends MongoRepository<MongoClientDetails, String>, MongoClientDetailsRepositoryBase {
}
