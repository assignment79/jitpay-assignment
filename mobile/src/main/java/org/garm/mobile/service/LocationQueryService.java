package org.garm.mobile.service;

import org.garm.common.model.dto.UserLocationDto;
import org.garm.infrastructure.base.domain.query.DefaultQueryService;
import org.garm.mobile.domain.UserLocationEntity;

public interface LocationQueryService extends DefaultQueryService<UserLocationEntity, UserLocationDto, Long> {
}
