package org.garm.mobile.service.impl;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.garm.common.model.dto.UserLocationDto;
import org.garm.mobile.mapper.JitPayLocationMapper;
import org.garm.mobile.repository.JitPayLocationRepository;
import org.garm.mobile.service.LocationCommandService;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Slf4j
@Component
@RequiredArgsConstructor
@Getter
public class LocationCommandServiceImpl implements LocationCommandService {

    private final JitPayLocationRepository repository;
    private final JitPayLocationMapper mapper;

    @Bean
    public Function<UserLocationDto, UserLocationDto> addLocation() {
        return (dto) -> {
            log.info("Received {}", dto);
            dto.setId(LocationCommandService.super.create(dto));
            log.info("Sending {}", dto);
            return dto;
        };
    }

}
