package org.garm.mobile.service.impl;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.garm.mobile.mapper.JitPayLocationMapper;
import org.garm.mobile.repository.JitPayLocationRepository;
import org.garm.mobile.service.LocationQueryService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Getter
public class LocationQueryServiceImpl implements LocationQueryService {

    private final JitPayLocationRepository repository;
    private final JitPayLocationMapper mapper;

}
