package org.garm.mobile.service;

import org.garm.common.model.dto.UserLocationDto;
import org.garm.infrastructure.base.domain.command.DefaultCommandService;
import org.garm.mobile.domain.UserLocationEntity;

public interface LocationCommandService extends DefaultCommandService<UserLocationEntity, UserLocationDto, Long> {
}