package org.garm.mobile.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.garm.assigment.cache.JitPayUserCacheService;
import org.garm.common.model.dto.JitPayUserDto;
import org.garm.common.model.dto.UserLocationDto;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.function.Consumer;

@Slf4j
@Component
@RequiredArgsConstructor
public class LocationCacheService {

    private final JitPayUserCacheService cacheService;

    @Bean
    public Consumer<UserLocationDto> addCache() {
        return (dto) -> {
            log.info("Received the value {} in Consumer", dto);
            JitPayUserDto jitPayUserDto = cacheService.get(dto.getUsername());
            if (!Objects.isNull(jitPayUserDto)) {
                jitPayUserDto.setLocation(dto);
                cacheService.put(dto.getUsername(), jitPayUserDto);
            }
        };
    }
}
