package org.garm.mobile.mapper;

import lombok.Getter;
import org.garm.common.model.dto.UserLocationDto;
import org.garm.infrastructure.base.mapper.BaseMapper;
import org.garm.mobile.domain.UserLocationEntity;
import org.garm.mobile.repository.JitPayLocationRepository;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.function.Supplier;

@Getter
@Mapper(componentModel = "spring")
public abstract class JitPayLocationMapper implements BaseMapper<UserLocationEntity, UserLocationDto, Long> {

    @Autowired
    private JitPayLocationRepository repository;

    @Override
    public Supplier<UserLocationEntity> getEntity() {
        return UserLocationEntity::new;
    }
}