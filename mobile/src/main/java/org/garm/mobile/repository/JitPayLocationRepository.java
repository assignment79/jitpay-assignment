package org.garm.mobile.repository;

import org.garm.infrastructure.base.domain.repository.BaseRepository;
import org.garm.mobile.domain.UserLocationEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface JitPayLocationRepository extends BaseRepository<UserLocationEntity, Long> {
}