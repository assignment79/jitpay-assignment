package org.garm.mobile.domain;

import lombok.Getter;
import lombok.Setter;
import org.garm.infrastructure.base.domain.AbstractAuditingEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import static org.garm.infrastructure.base.domain.AbstractPersistence.ALLOCATION_SIZE;
import static org.garm.infrastructure.base.domain.AbstractPersistence.DEFAULT_SEQ_GEN;


@Getter
@Setter
@Entity
@Table(name = UserLocationEntity.TABLE_NAME, schema = "JT")
@SequenceGenerator(name = DEFAULT_SEQ_GEN, sequenceName = UserLocationEntity.TABLE_NAME + "_SEQ", allocationSize = ALLOCATION_SIZE)
public class UserLocationEntity extends AbstractAuditingEntity<Long> {

    public static final String TABLE_NAME = "JT_LOCATION";

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "latitude", nullable = false)
    private double latitude;

    @Column(name = "longitude", nullable = false)
    private double longitude;

}

