package org.garm.mobile.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.garm.common.model.dto.UserLocationDto;
import org.garm.infrastructure.base.model.service.Response;
import org.garm.infrastructure.base.utils.SecurityUtils;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/location")
public class LocationCommandController {
    private final StreamBridge streamBridge;

    @PutMapping("v1/add")
    public Response<UserLocationDto, Long> add(@RequestBody UserLocationDto dto) {
        log.info("Sending value {} to topic", dto);
        dto.setUsername(SecurityUtils.getAuthDto().getUsername());
        streamBridge.send("add-location-topic", dto);
        Response<UserLocationDto, Long> response = new Response<>();
        response.setInputArguments(dto);
        return response;
    }

}