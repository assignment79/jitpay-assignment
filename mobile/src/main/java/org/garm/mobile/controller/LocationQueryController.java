package org.garm.mobile.controller;

import lombok.RequiredArgsConstructor;
import org.garm.mobile.service.LocationQueryService;
import org.garm.infrastructure.base.dto.PagingRequest;
import org.garm.infrastructure.base.dto.PagingResponse;
import org.garm.infrastructure.base.model.service.Response;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/location")
public class LocationQueryController {

    private final LocationQueryService service;

    @PostMapping("v1/find-paging")
    @ResponseBody
    public Response<PagingRequest, PagingResponse> findPaging(@RequestBody PagingRequest pagingRequest) {
        Response<PagingRequest, PagingResponse> response = new Response<>();
        response.setResponse(service.findPaging(pagingRequest));
        return response;
    }

}
