# JITpay assignment

## Getting started

this project built on top of spring framework and java 11. <br />
infrastructure base and security project are parent project, this is mine custom framework to help me and my colleague <br />
to develop projects faster and with unique structure. <br />

first of all you should run install.sh in installer project to install requirement for this project to run. <br />

## install requirement

1 - cd /jitpay-assignment/installer <br />
2 - sh install.sh <br />

## uninstall requirement

for uninstalling this project you can run bellow script: <br />

1 - cd /jitpay-assignment/installer <br />
2 - sh uninstall.sh <br />

## build project

after install requirement you can run this project with bellow instruction: <br />

1 - cd /jitpay-assignment <br />
2 - mvn clean install -DskipTests <br />

## run project

1 - first run api-gateway project <br />
2 - second run assignment project <br />
2 - third run mobile project <br />

## import and run project from postman

5 - you can import assignment-garmestani.postman_collection.json to postman <br />
6 - call api from postman and finish <br />

## deploy project with docker

for installing this project you can use bellow command: <br />

## install api gateway
1 - cd /jitpay-assignment/api-gateway <br />
2 - docker build -t api-gateway . <br />
3 - docker run -itd --name api-gateway -p 8080:8080 --net garm-net api-gateway <br />

## install api assigment
4 - cd jitpay-assignment/assigment <br />
5 - docker build -t assigment . <br />
6 - docker run -itd --name assigment --net garm-net assigment <br />

## install mobile
4 - cd jitpay-assignment/mobile <br />
5 - docker build -t mobile . <br />
6 - docker run -itd --name mobile --net garm-net mobile <br />